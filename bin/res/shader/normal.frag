uniform sampler2D texture;
uniform vec2 textureSize;

void main()
{
    // récupère le pixel dans la texture
    vec4 pixel = texture2D(texture, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y).xy);

    // et multiplication avec la couleur pour obtenir le pixel final
    gl_FragColor = gl_Color * pixel;
}
