uniform sampler2D texture;
uniform vec2 textureSize;
uniform float amplitude;


float interpolation(float A, float B, float C, float D, float t, float t2, float t3)
{
    float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
    float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
    float c = -A / 2.0f + C / 2.0f;
    float d = B;
 
    return a*t3 + b*t2 + c*t + d;
}


void main()
{
    float fX = gl_TexCoord[0].x * textureSize.x - floor(gl_TexCoord[0].x * textureSize.x);
    float fY = gl_TexCoord[0].y * textureSize.y - floor(gl_TexCoord[0].y * textureSize.y);

    float fX2 = fX*fX;
    float fX3 = fX2*fX;

    float fY2 = fY*fY;
    float fY3 = fY2*fY;

    float unit = 1.0 / (float)textureSize.x;
    float unit2 = unit * 2.0;

    vec4 pixel;

    float c00 = texture2D(texture, vec2(gl_TexCoord[0].x - unit , gl_TexCoord[0].y - unit).xy).r;
    float c01 = texture2D(texture, vec2(gl_TexCoord[0].x        , gl_TexCoord[0].y - unit).xy).r;
    float c02 = texture2D(texture, vec2(gl_TexCoord[0].x + unit , gl_TexCoord[0].y - unit).xy).r;
    float c03 = texture2D(texture, vec2(gl_TexCoord[0].x + unit2, gl_TexCoord[0].y - unit).xy).r;

    float c10 = texture2D(texture, vec2(gl_TexCoord[0].x - unit , gl_TexCoord[0].y).xy).r;
    float c11 = texture2D(texture, vec2(gl_TexCoord[0].x        , gl_TexCoord[0].y).xy).r;
    float c12 = texture2D(texture, vec2(gl_TexCoord[0].x + unit , gl_TexCoord[0].y).xy).r;
    float c13 = texture2D(texture, vec2(gl_TexCoord[0].x + unit2, gl_TexCoord[0].y).xy).r;

    float c20 = texture2D(texture, vec2(gl_TexCoord[0].x - unit , gl_TexCoord[0].y + unit).xy).r;
    float c21 = texture2D(texture, vec2(gl_TexCoord[0].x        , gl_TexCoord[0].y + unit).xy).r;
    float c22 = texture2D(texture, vec2(gl_TexCoord[0].x + unit , gl_TexCoord[0].y + unit).xy).r;
    float c23 = texture2D(texture, vec2(gl_TexCoord[0].x + unit2, gl_TexCoord[0].y + unit).xy).r;

    float c30 = texture2D(texture, vec2(gl_TexCoord[0].x - unit , gl_TexCoord[0].y + unit2).xy).r;
    float c31 = texture2D(texture, vec2(gl_TexCoord[0].x        , gl_TexCoord[0].y + unit2).xy).r;
    float c32 = texture2D(texture, vec2(gl_TexCoord[0].x + unit , gl_TexCoord[0].y + unit2).xy).r;
    float c33 = texture2D(texture, vec2(gl_TexCoord[0].x + unit2, gl_TexCoord[0].y + unit2).xy).r;

    float col0 = interpolation(c00, c01, c02, c03, fX, fX2, fX3);
    float col1 = interpolation(c10, c11, c12, c13, fX, fX2, fX3);
    float col2 = interpolation(c20, c21, c22, c23, fX, fX2, fX3);
    float col3 = interpolation(c30, c31, c32, c33, fX, fX2, fX3);

    float value = interpolation(col0, col1, col2, col3, fY, fY2, fY3) * amplitude;

    pixel = vec4(value, value, value, 1.0);
    


    // et multiplication avec la couleur pour obtenir le pixel final
    gl_FragColor = gl_Color * pixel;
}
