uniform sampler2D texture;
uniform int nbPoint;
uniform float positions[64];
uniform vec4 colors[64];

void main()
{
    float refColor = texture2D(texture, vec2(gl_TexCoord[0].x, gl_TexCoord[0].y).xy).r;
    int index = 0;
    bool loop = true;

    vec4 pixel;

    while (index < nbPoint && loop) {
        if (refColor > positions[index]) {
            index++;
        } else {
            loop = false;
        }
    }

    /*
    if (refColor < 0.5) {
        pixel = colors[2];
    } else {
        pixel = colors[3];
    }*/

    //pixel.r = refColor;

    if (refColor == positions[index]) {
        //pixel = vec4(1.f, 1.f, 1.f, 1.f);    
        pixel = colors[index];
    } else {
        float diff = positions[index] - positions[index - 1];
        float ratio = (positions[index] - refColor) / diff;
        float invertRatio = 1.f - ratio;
        vec4 cA = colors[index];
        vec4 cB = colors[index-1];
        pixel = vec4(cB.r * ratio + cA.r * invertRatio, cB.g * ratio + cA.g * invertRatio, cB.b * ratio + cA.b * invertRatio, 1.f);
    }

    // et multiplication avec la couleur pour obtenir le pixel final
    gl_FragColor = gl_Color * pixel;
}
