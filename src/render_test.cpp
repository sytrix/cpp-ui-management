#include "flex/xml.tab.h"

#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <iostream>

#include "render/NodeNoise.hpp"


#define RENDER_WIDTH        1024
#define RENDER_HEIGHT       1024


sf::Shader shaderNoise;
sf::Shader shaderBicubic;

sf::RenderTexture renderTextureA;

void newNoiseMap(sf::RenderTexture *result, int renderWidth, int renderHeight, float ratio, float amplitude, float seed)
{
    sf::Clock clock;
    clock.restart();
    int texW = renderWidth * ratio;
    int texH = renderHeight * ratio;
    shaderNoise.setUniform("time", seed);
    //renderTextureA.draw(sf::RectangleShape(sf::Vector2f(2, 2)));
    renderTextureA.clear();
    //std::cout << "clear A elapsed time : " << clock.getElapsedTime().asSeconds() << std::endl;


    clock.restart();
    sf::RenderTexture renderTextureC;
    renderTextureC.create(texW, texH);

    renderTextureC.draw(sf::RectangleShape(sf::Vector2f(texW, texH)), &shaderNoise);
    //std::cout << "noise elapsed time : " << clock.getElapsedTime().asSeconds() << std::endl;


    clock.restart();    
    shaderBicubic.setUniform("texture", sf::Shader::CurrentTexture);
    shaderBicubic.setUniform("textureSize", sf::Glsl::Vec2(texW, texH));
    shaderBicubic.setUniform("amplitude", amplitude);

    sf::Texture texture(renderTextureC.getTexture());
    texture.setRepeated(true);
    sf::Sprite sprite(texture);
    sprite.setScale(1.f / ratio, 1.f / ratio);
    sprite.setTextureRect(sf::IntRect(0, texH, texW, -texH));
    

    
    sf::RenderTexture renderTextureB;
    renderTextureB.create(renderWidth, renderHeight);
    renderTextureB.draw(sprite, &shaderBicubic);
    //std::cout << "bicubic elapsed time : " << clock.getElapsedTime().asSeconds() << std::endl;

    clock.restart();
    result->draw(sf::Sprite(renderTextureB.getTexture()), sf::BlendAdd);
    //std::cout << "draw : " << clock.getElapsedTime().asSeconds() << std::endl;
}

int main(int argc, char *argv[])
{
    xml_element_array *root = parseXmlFile("res/test.xml");

    /*sf::Texture texture;
    texture.loadFromFile("res/image/cute.png");
    sf::Sprite sprite(texture);
    sprite.setScale(RENDER_WIDTH / (float)textureSize.x, RENDER_HEIGHT / (float)textureSize.y);
    sprite.setTextureRect(sf::IntRect(0, textureSize.y, textureSize.x, -textureSize.y));*/

    // charge les deux
    if (!shaderNoise.loadFromFile("res/shader/noise.frag", sf::Shader::Fragment))
    {
        // erreur...
        std::cerr << "shader load error " << std::endl;
        return 1;
    }

    // charge les deux
    if (!shaderBicubic.loadFromFile("res/shader/bicubic.frag", sf::Shader::Fragment))
    {
        // erreur...
        std::cerr << "shader load error " << std::endl;
        return 1;
    }

    //shader.setUniform("texture", sf::Shader::CurrentTexture);
    //shader.setUniform("textureSize", sf::Glsl::Vec2(0.1f, 0.1f));
    
    sf::Clock clockTotal;
    clockTotal.restart();

    sf::Clock clockInit;
    clockInit.restart();
    renderTextureA.create(1, 1);
    renderTextureA.clear();
    std::cout << "clock init : " << clockInit.getElapsedTime().asSeconds() << std::endl;
    
    clockInit.restart();
    sf::RenderTexture renderTexture;
    renderTexture.create(RENDER_WIDTH, RENDER_HEIGHT);
    std::cout << "create final render : " << clockInit.getElapsedTime().asSeconds() << std::endl;

    clockInit.restart();
    newNoiseMap(&renderTexture, RENDER_WIDTH, RENDER_HEIGHT, 1.f / 32.f, 0.05f, 165.12f);
    std::cout << "3 noise : " << clockInit.getElapsedTime().asSeconds() << std::endl;
    clockInit.restart();
    newNoiseMap(&renderTexture, RENDER_WIDTH, RENDER_HEIGHT, 1.f / 128.f, 0.25f, 604.12f);
    
    newNoiseMap(&renderTexture, RENDER_WIDTH, RENDER_HEIGHT, 1.f / 256.f, 0.7f, 856.12f);
    std::cout << "3 noise : " << clockInit.getElapsedTime().asSeconds() << std::endl;
    std::cout << "clock total : " << clockTotal.getElapsedTime().asSeconds() << std::endl;

    sf::Image image(renderTexture.getTexture().copyToImage());
    image.saveToFile("render.png");


    freeXmlStruct(root);

    return 0;
}

