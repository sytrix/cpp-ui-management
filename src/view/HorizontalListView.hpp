#ifndef DEF_HORIZONTALLISTVIEW_H
#define DEF_HORIZONTALLISTVIEW_H

#include "ListView.hpp"

class HorizontalListView : virtual public ListView
{
    public:

        HorizontalListView(float interSpace);

    protected:

        virtual sf::Vector2f nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index);
    
    private:

        float m_interSpace;



};

#endif

