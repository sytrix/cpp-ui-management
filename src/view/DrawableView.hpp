#ifndef DEF_DRAWABLEVIEW_H
#define DEF_DRAWABLEVIEW_H

#include <SFML/Graphics.hpp>
#include <vector>

class RoundedRectangle;

enum DisplayMode {
    FitToSuperview,
    Fix,
    FitToChildrens
};

enum Alignement {
    Left,
    Center,
    Right,
    Top,
    Bottom,
};

class DrawableView : virtual public sf::Drawable
{
    friend class ListView;

    public:

        DrawableView();
        ~DrawableView();

        void setViewBackgroundColor(sf::Color color);
        sf::Color getViewBackgroundColor();
        
        void setViewOutlineColor(sf::Color color);
        void setViewOutlineThickness(float thickness);
        void setViewSize(float x, float y);
        void setViewSize(sf::Vector2f size);
        void setViewMargin(sf::Vector2f marginTopLeft, sf::Vector2f marginBottomRight);
        void setViewPadding(float x, float y);
        void setViewPadding(sf::Vector2f padding);
        void setViewPadding(sf::Vector2f paddingTopLeft, sf::Vector2f paddingBottomRight);
        void setViewCornerRadius(float cornerRadius);
        void setViewAllowOverflow(bool overflow);
        void setViewWidth(float width);
        void setViewHeight(float height);
        void setHorizontalDisplayMode(DisplayMode displayMode);
        void setVerticalDisplayMode(DisplayMode displayMode);

        

        virtual bool updateEvent(sf::Event &event);

        sf::FloatRect layoutContent(sf::FloatRect frame);
        std::vector<DrawableView *> *getListOfInteractableView();
        void makePreRenderIfNeeded();


    protected:

        void needRender();

        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
        virtual void drawView(sf::RenderTarget &target, sf::RenderStates states) const;

        virtual void fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView);

        virtual bool checkIfNeedPreRender();
        virtual void preRender();
        virtual sf::Vector2f layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize);
        virtual sf::Vector2f layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode);
        virtual void setDrawableFrame(float x, float y, float width, float height);

        virtual float maxWidthChild();
        virtual float maxHeightChild(float width);

        DisplayMode m_horizontalDisplayMode;
        DisplayMode m_verticalDisplayMode;
        sf::Vector2f m_fixedSize;
        sf::Vector2f m_marginTopLeft;
        sf::Vector2f m_marginBottomRight;
        sf::Vector2f m_paddingTopLeft;
        sf::Vector2f m_paddingBottomRight;
        sf::Vector2f m_drawableSize;
        sf::Vector2f m_drawablePosition;

        float m_cornerRadius;
        bool m_allowOverflow;
        bool m_needRender;
        
    
    private:

        sf::Texture *m_overflowTexture;
        sf::Sprite *m_overflowSprite;
        RoundedRectangle *m_roundedRectangle;
        
        
        


        



};

#endif

