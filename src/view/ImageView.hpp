#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include "DrawableView.hpp"

class ImageView : virtual public DrawableView
{
	public:
		ImageView();
		~ImageView();
		
		void setImageViewTexture(sf::Texture &texture);
        void setImageTintColor(sf::Color tintColor);

		
	protected:

		virtual void drawView(sf::RenderTarget &target, sf::RenderStates states) const;

        virtual void preRender();
        virtual void setDrawableFrame(float x, float y, float width, float height);

        virtual float maxWidthChild();
        virtual float maxHeightChild(float width);
		
	private:

        sf::Texture *m_originalTexture;
        sf::Texture *m_renderedTexture;
		sf::Sprite m_sprite;
        sf::Color m_tintColor;
		Alignement m_horizonalAlignement;
        Alignement m_verticalAlignement;
};

#endif

