#include "HorizontalListView.hpp"

HorizontalListView::HorizontalListView(float interSpace)
:
    ListView::ListView(),
    m_interSpace(interSpace)
{
    //m_allowOverflow = false;
}

sf::Vector2f HorizontalListView::nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index) 
{
    return sf::Vector2f(previousChildShift.x + previousChildSize.x + m_interSpace, 0);
}

