#include "ButtonView.hpp"
#include "ListView.hpp"


ButtonView::ButtonView()
:
    ListView(),
    m_defaultColor(sf::Color::Transparent),
    m_overColor(sf::Color::Transparent)
{

}

void ButtonView::setButtonDefaultColor(Color color)
{
    this->setViewBackgroundColor(color);
    m_defaultColor = color;
}

void ButtonView::setButtonOverColor(Color color)
{
    m_overColor = color;
}

void ButtonView::fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView)
{
    listInteractableView.push_back(this);
}

bool ButtonView::checkIfNeedPreRender()
{
    return ListView::checkIfNeedPreRender();
}

bool ButtonView::updateEvent(sf::Event &event)
{
    if (event.type == sf::Event::MouseMoved) {
        float x = event.mouseMove.x;
        float y = event.mouseMove.y;

        sf::FloatRect frame(m_drawablePosition, m_drawableSize);
        sf::Color previousColor(this->getViewBackgroundColor());
        sf::Color nextColor;
        if(frame.contains(x, y)) {
            nextColor = m_overColor;
        } else {
            nextColor = m_defaultColor;
        }

        this->setViewBackgroundColor(nextColor);

        if (nextColor != previousColor) {
            this->needRender();
        }
    }
    if (event.type == sf::Event::MouseButtonPressed) {
        float x = event.mouseButton.x;
        float y = event.mouseButton.y;

        sf::FloatRect frame(m_drawablePosition, m_drawableSize);
        if(frame.contains(x, y)) {
            return true;
        } else {
            return false;
        }
    }

    return false;
}