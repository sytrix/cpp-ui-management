#ifndef DEF_BUTTONVIEW_H
#define DEF_BUTTONVIEW_H

#include "ListView.hpp"

class ButtonView : virtual public ListView
{
    public:

        ButtonView();

        void setButtonDefaultColor(Color color);
        void setButtonOverColor(Color color);

        virtual bool updateEvent(sf::Event &event);

    protected:

        virtual void fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView);

        virtual bool checkIfNeedPreRender();
    
    private:

        sf::Color m_defaultColor;
        sf::Color m_overColor;



};

#endif

