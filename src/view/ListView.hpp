#ifndef DEF_LISTVIEW_H
#define DEF_LISTVIEW_H

#include "DrawableView.hpp"

using namespace sf;

class ListView : virtual public DrawableView
{
    public:

        ListView();
        ~ListView();

        void addSubview(DrawableView *view);

        sf::FloatRect getViewRect();

        void setHorizontalAlignement(Alignement alignement);
        void setVerticalAlignement(Alignement alignement);

    protected:

        virtual void fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView);

        virtual sf::Vector2f nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index);
        virtual float maxWidthChild();
        virtual float maxHeightChild(float width);

        virtual bool checkIfNeedPreRender();
        virtual void preRender();
        virtual sf::Vector2f layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize);
        virtual sf::Vector2f layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode);

        virtual void drawView(RenderTarget &target, RenderStates states) const;

        std::vector<DrawableView *> m_listView;
        Alignement m_horizontalAlignement;
        Alignement m_verticalAlignement;
        

    private:

        float m_maxWidth;
        float m_maxHeight;
};

#endif

