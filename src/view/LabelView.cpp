#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

#include "LabelView.hpp"

LabelView::LabelView(const sf::String& string, const sf::Font& font, unsigned int characterSize)
:   
    DrawableView::DrawableView(),
    m_horizonalAlignement(Left),
    m_verticalAlignement(Top),
    m_label(string, font, characterSize)
{
    

}

void LabelView::setHorizontalAlignement(Alignement alignement)
{
    m_horizonalAlignement = alignement;
}

void LabelView::setVerticalAlignement(Alignement alignement)
{
    m_verticalAlignement = alignement;
}

Label &LabelView::getLabel()
{
    return m_label;
}

void LabelView::setDrawableFrame(float x, float y, float width, float height)
{
    //std::cout << " height : " << height << std::endl;
    DrawableView::setDrawableFrame(x, y, width, height);

    int mode = 0;
    if (m_verticalDisplayMode == DisplayMode::FitToChildrens) {
        mode = 1;
    }
     
    m_label.fit(width - m_paddingTopLeft.x - m_paddingBottomRight.x, height - m_paddingTopLeft.y - m_paddingBottomRight.y, mode);

    sf::FloatRect localBounds = m_label.getLocalBounds();
    m_drawableLabelSize = sf::Vector2f(localBounds.width, localBounds.height);

    float originX;
    float originY;

    if (m_horizonalAlignement == Alignement::Left) {
        originX = x + m_paddingTopLeft.x;
    } else if (m_horizonalAlignement == Alignement::Center) {
        originX = x + width / 2 - localBounds.width / 2;
    } else if (m_horizonalAlignement == Alignement::Right) {
        originX = x + width - localBounds.width - m_paddingBottomRight.x;
    } 

     if (m_verticalAlignement == Alignement::Top) {
        originY = y + m_paddingTopLeft.y;
    } else if (m_verticalAlignement == Alignement::Center) {
        originY = y + height / 2 - localBounds.height / 2;
    } else if (m_verticalAlignement == Alignement::Bottom) {
        originY = y + height - localBounds.height - m_paddingBottomRight.y;
    } 

    originY -= 5;


    m_label.setPosition(roundf(originX), roundf(originY));
}

float LabelView::maxWidthChild() 
{
    return m_label.getLocalBounds().width;
}

float LabelView::maxHeightChild(float width) 
{
    int mode = 0;
    if (m_verticalDisplayMode == DisplayMode::FitToChildrens) {
        mode = 1;
    }

    m_label.fit(width - m_paddingTopLeft.x - m_paddingBottomRight.x);
    return m_label.getLocalBounds().height + m_paddingTopLeft.y + m_paddingBottomRight.y + 5;
}

void LabelView::drawView(sf::RenderTarget &target, sf::RenderStates states) const
{
    DrawableView::drawView(target, states);
    
    target.draw(m_label, states);
}

