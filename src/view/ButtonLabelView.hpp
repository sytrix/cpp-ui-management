#ifndef DEF_BUTTONLABELVIEW_H
#define DEF_BUTTONLABELVIEW_H

#include "LabelView.hpp"
#include "ButtonView.hpp"

class ButtonLabelView : public LabelView, public ButtonView
{
    public:

        ButtonLabelView(const String& string, const Font& font, unsigned int characterSize = 30);

        void setButtonLabelDefaultColor(Color color);
        void setButtonLabelOverColor(Color color);

        sf::FloatRect getViewRect();

        void setHorizontalAlignement(Alignement alignement);
        void setVerticalAlignement(Alignement alignement);
        

    protected:

        virtual void fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView);

        virtual void drawView(RenderTarget &target, RenderStates states) const;

        virtual bool checkIfNeedPreRender();
        virtual sf::Vector2f layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize);
        virtual sf::Vector2f layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode);
        virtual void setDrawableFrame(float x, float y, float width, float height);

        virtual float maxWidthChild();
        virtual float maxHeightChild(float width);
    
    private:

        sf::Color m_labelDefaultColor;
        sf::Color m_labelOverColor;



};

#endif

