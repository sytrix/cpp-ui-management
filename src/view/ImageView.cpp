#include "ImageView.hpp"

#include <cmath>

#include "../drawable/RoundedRectangle.hpp"



ImageView::ImageView() 
: 
    m_originalTexture(nullptr),
    m_renderedTexture(nullptr),
    m_tintColor(sf::Color::White),
    m_horizonalAlignement(Alignement::Left), 
    m_verticalAlignement(Alignement::Top)
{
	m_needRender = true;
}

ImageView::~ImageView()
{
    delete m_renderedTexture;
}

void ImageView::setImageViewTexture(sf::Texture &texture)
{
    m_originalTexture = &texture;
    m_sprite.setTexture(texture);
}

void ImageView::setImageTintColor(sf::Color tintColor)
{
    m_tintColor = tintColor;
}

void ImageView::setDrawableFrame(float x, float y, float width, float height)
{
    DrawableView::setDrawableFrame(x, y, width, height);

    float textureWidth = m_sprite.getTexture()->getSize().x;
    float textureHeight = m_sprite.getTexture()->getSize().y;

    float contentWidth = width - m_paddingTopLeft.x - m_paddingBottomRight.x;
    float contentHeight = height - m_paddingTopLeft.y - m_paddingBottomRight.y;

    float finalWidth = contentWidth;
    float finalHeight = contentHeight;
    
    m_sprite.setScale(finalWidth / textureWidth, finalHeight / textureHeight);

    float originX;
    float originY;

    if (m_horizonalAlignement == Alignement::Left) {
        originX = x + m_paddingTopLeft.x;
    } else if (m_horizonalAlignement == Alignement::Center) {
        originX = x + width / 2 - finalWidth / 2;
    } else if (m_horizonalAlignement == Alignement::Right) {
        originX = x + width - finalWidth - m_paddingBottomRight.x;
    } 

     if (m_verticalAlignement == Alignement::Top) {
        originY = y + m_paddingTopLeft.y;
    } else if (m_verticalAlignement == Alignement::Center) {
        originY = y + height / 2 - finalHeight / 2;
    } else if (m_verticalAlignement == Alignement::Bottom) {
        originY = y + height - finalHeight - m_paddingBottomRight.y;
    }

    m_sprite.setPosition(roundf(originX), roundf(originY));
}

float ImageView::maxWidthChild() 
{
    return m_sprite.getTexture()->getSize().x;
}

float ImageView::maxHeightChild(float width) 
{
    float textureWidth = m_sprite.getTexture()->getSize().x;
    float textureHeight = m_sprite.getTexture()->getSize().y;

    return width * textureHeight / textureWidth;
}

void ImageView::preRender()
{
    if (m_needRender) {

        delete m_renderedTexture;

        sf::RenderTexture renderTexture;
        sf::Vector2u textureSize = m_originalTexture->getSize();
        renderTexture.create(textureSize.x, textureSize.y);

        float ratioX = textureSize.x / m_drawableSize.x;
        float ratioY = textureSize.y / m_drawableSize.y;
        RoundedRectangle roundedRectangle;
        roundedRectangle.makeRoundedRectangle(textureSize.x, textureSize.y, m_cornerRadius * ratioX, m_cornerRadius * ratioY);
        roundedRectangle.setFillColor(m_tintColor);

        sf::Sprite sprite(*m_originalTexture);
        sprite.setTextureRect(sf::IntRect(0, textureSize.y, textureSize.x, -textureSize.y));

        renderTexture.clear(sf::Color::Transparent);
        renderTexture.draw(roundedRectangle, sf::RenderStates::Default);
        renderTexture.draw(sprite, sf::RenderStates(sf::BlendMultiply));

        m_renderedTexture = new sf::Texture(renderTexture.getTexture());
        m_sprite.setTexture(*m_renderedTexture);

    }

    DrawableView::preRender();
}

void ImageView::drawView(sf::RenderTarget &target, sf::RenderStates states) const
{
    DrawableView::drawView(target, states);

    target.draw(m_sprite, states);
}