#ifndef DEF_LABELVIEW_H
#define DEF_LABELVIEW_H

#include "DrawableView.hpp"
#include "../drawable/Label.hpp"

class LabelView : virtual public DrawableView
{
    public:

        LabelView(const sf::String& string, const sf::Font& font, unsigned int characterSize = 30);

        void setHorizontalAlignement(Alignement alignement);
        void setVerticalAlignement(Alignement alignement);

        Label &getLabel();
        

    protected:

        virtual void drawView(sf::RenderTarget &target, sf::RenderStates states) const;

        virtual void setDrawableFrame(float x, float y, float width, float height);

        virtual float maxWidthChild();
        virtual float maxHeightChild(float width);

        Alignement m_horizonalAlignement;
        Alignement m_verticalAlignement;
        sf::Vector2f m_drawableLabelSize;
    
    private:

        Label m_label;
        



};

#endif

