#include <SFML/Graphics.hpp>
#include "DrawableView.hpp"
#include <iostream>
#include "../drawable/RoundedRectangle.hpp"
#include <cmath>
#include <vector>

DrawableView::DrawableView() 
:
    m_horizontalDisplayMode(DisplayMode::FitToSuperview),
    m_verticalDisplayMode(DisplayMode::FitToChildrens),
    m_fixedSize(-1.f, -1.f),
    m_cornerRadius(15.f),
    m_allowOverflow(true),
    m_needRender(false),

    m_overflowTexture(nullptr),
    m_overflowSprite(nullptr),
    m_roundedRectangle(nullptr)
    
{  
}

DrawableView::~DrawableView()
{
    delete m_overflowTexture;
    delete m_overflowSprite; 
    delete m_roundedRectangle;
}

void DrawableView::setViewBackgroundColor(sf::Color color)
{
    if (m_roundedRectangle == nullptr) {
        m_roundedRectangle = new RoundedRectangle();
    }

    m_roundedRectangle->setFillColor(color);
}

sf::Color DrawableView::getViewBackgroundColor()
{
    if (m_roundedRectangle == nullptr) {
        return sf::Color::Transparent;
    }

    return m_roundedRectangle->getFillColor();
}

void DrawableView::setViewOutlineColor(sf::Color color)
{
    if (m_roundedRectangle == nullptr) {
        m_roundedRectangle = new RoundedRectangle();
    }

    m_roundedRectangle->setOutlineColor(color);
}

void DrawableView::setViewOutlineThickness(float thickness)
{
    if (m_roundedRectangle == nullptr) {
        m_roundedRectangle = new RoundedRectangle();
    }

    m_roundedRectangle->setOutlineThickness(thickness);
}

void DrawableView::setViewMargin(sf::Vector2f marginTopLeft, sf::Vector2f marginBottomRight)
{
    m_marginTopLeft = marginTopLeft;
    m_marginBottomRight = marginBottomRight;
}

void DrawableView::setViewPadding(float x, float y)
{
    m_paddingTopLeft.x = x;
    m_paddingTopLeft.y = y;
    m_paddingBottomRight.x = x;
    m_paddingBottomRight.y = y;
}

void DrawableView::setViewPadding(sf::Vector2f padding) 
{
    m_paddingTopLeft = padding;
    m_paddingBottomRight = padding;
}

void DrawableView::setViewPadding(sf::Vector2f paddingTopLeft, sf::Vector2f paddingBottomRight)
{
    m_paddingTopLeft = paddingTopLeft;
    m_paddingBottomRight = paddingBottomRight;
}

void DrawableView::setViewSize(float x, float y)
{
    m_fixedSize.x = x;
    m_fixedSize.y = y;
}

void DrawableView::setViewSize(sf::Vector2f size) {this->setViewSize(size.x, size.y);}

void DrawableView::setHorizontalDisplayMode(DisplayMode displayMode)
{
    m_horizontalDisplayMode = displayMode;
}

void DrawableView::setVerticalDisplayMode(DisplayMode displayMode)
{
    m_verticalDisplayMode = displayMode;
}

void DrawableView::setViewCornerRadius(float cornerRadius)
{
    m_cornerRadius = cornerRadius;
}

void DrawableView::setViewAllowOverflow(bool allowOverflow)
{
    m_allowOverflow = allowOverflow;
}

void DrawableView::setViewWidth(float width)
{
    m_fixedSize.x = width;
}

void DrawableView::setViewHeight(float height)
{
    m_fixedSize.y = height;
}

bool DrawableView::updateEvent(sf::Event &event)
{
    return false;
}

sf::FloatRect DrawableView::layoutContent(sf::FloatRect frame)
{
    m_fixedSize.x = frame.width;
    m_fixedSize.y = frame.height;
    sf::Vector2f childSize(
        frame.width - m_marginTopLeft.x - m_marginBottomRight.x, 
        frame.height - m_marginTopLeft.y - m_marginBottomRight.y
    );
    this->layoutSize(childSize, DisplayMode::Fix, DisplayMode::Fix);
    this->layoutPosition(sf::Vector2f(frame.left + m_marginTopLeft.x, frame.top + m_marginTopLeft.y), sf::Vector2f(frame.width - m_marginTopLeft.x - m_marginBottomRight.x, frame.height - m_marginTopLeft.y - m_marginBottomRight.y));
    this->preRender();

    sf::FloatRect viewport = sf::FloatRect(frame.left, frame.top, m_drawableSize.x, m_drawableSize.y);

    return viewport;
}

std::vector<DrawableView *> *DrawableView::getListOfInteractableView()
{
    std::vector<DrawableView *> *result = new std::vector<DrawableView *>();

    this->fillListWithInteractableView(*result);

    return result;
}

void DrawableView::makePreRenderIfNeeded()
{
    bool needPreRender = this->checkIfNeedPreRender();

    if (needPreRender) {
        this->preRender();
    }
}

void DrawableView::needRender()
{
    m_needRender = true;
}

void DrawableView::fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView)
{

}

bool DrawableView::checkIfNeedPreRender()
{
    if (m_needRender) {
        m_needRender = false;
        return true;
    }

    return false;
}

void DrawableView::preRender()
{
    if (!m_allowOverflow) {
        delete m_overflowTexture; m_overflowTexture = nullptr;

        if (m_drawableSize.x > 0.f && m_drawableSize.y > 0.f) {
           sf::RenderTexture renderTexture;
            renderTexture.create(m_drawableSize.x, m_drawableSize.y);
            sf::View view(sf::FloatRect(m_drawablePosition.x, m_drawablePosition.y, m_drawableSize.x, m_drawableSize.y));
            renderTexture.setView(view);

            this->drawView(renderTexture, sf::RenderStates::Default);

            m_overflowTexture = new sf::Texture(renderTexture.getTexture());
            m_overflowSprite->setTexture(*m_overflowTexture);
        } else {
            m_overflowSprite->setTexture(sf::Texture());
        }
    }
}

sf::Vector2f DrawableView::layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize)
{
    this->setDrawableFrame(topLeftCorner.x, topLeftCorner.y, m_drawableSize.x, m_drawableSize.y);

    m_drawablePosition = topLeftCorner;

    return m_drawablePosition;
}

sf::Vector2f DrawableView::layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode)
{
    float width = 0.f;
    float height = 0.f;

    if (m_horizontalDisplayMode == DisplayMode::FitToSuperview) {
        if (parentHorizontalDisplayMode == DisplayMode::FitToChildrens) {
            std::cerr << "[Interface Layout Warning] horizontal can't fit two superview themself : fit to superview in fit to child" << std::endl;
        } else {
            width = sizeToFit.x;
        }
    } else if (m_horizontalDisplayMode == DisplayMode::Fix) {
        if (m_fixedSize.x == -1.f) {
            std::cerr << "[Interface Layout Warning] fixed width not define" << std::endl;
        } else {
            width = m_fixedSize.x;
        }
    } else if (m_horizontalDisplayMode == DisplayMode::FitToChildrens) {
        width = this->maxWidthChild();
    }

    

    if (m_verticalDisplayMode == DisplayMode::FitToSuperview) {
        if (parentHorizontalDisplayMode == DisplayMode::FitToChildrens) {
            std::cerr << "[Interface Layout Warning] vertical can't fit two superview themself : fit to superview in fit to child" << std::endl;
        } else {
            height = sizeToFit.y;
        }
    } else if (m_verticalDisplayMode == DisplayMode::Fix) {
        if (m_fixedSize.y == -1.f) {
            std::cerr << "[Interface Layout Warning] fixed height not define" << std::endl;
        } else {
            height = m_fixedSize.y;
        }
    } else if (m_verticalDisplayMode == DisplayMode::FitToChildrens) {
        height = this->maxHeightChild(width);
    }

    m_drawableSize = sf::Vector2f(width, height);

    return m_drawableSize;
}

void DrawableView::setDrawableFrame(float x, float y, float width, float height)
{
    float roundX = roundf(x);
    float roundY = roundf(y);

    if (m_roundedRectangle != nullptr) {
        m_roundedRectangle->setPosition(roundX, roundY);
        m_roundedRectangle->makeRoundedRectangle(width, height, m_cornerRadius);
    }

    if (!m_allowOverflow) {
        delete m_overflowTexture; m_overflowTexture = nullptr;
        delete m_overflowSprite; m_overflowSprite = nullptr;

        m_overflowSprite = new sf::Sprite();
        m_overflowSprite->setPosition(roundX, roundY);
        m_overflowSprite->setTextureRect(sf::IntRect(0, height, width, -height));
    }
}

float DrawableView::maxWidthChild() 
{
    return 0.f;
}

float DrawableView::maxHeightChild(float width) 
{
    return 0.f;
}

void DrawableView::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_drawableSize.x > 0.f && m_drawableSize.y > 0.f) {
        if (m_allowOverflow) {
            this->drawView(target, states);
        } else {
            if (m_overflowSprite) {
                target.draw(*m_overflowSprite, states);
            }
        }
    }
}

void DrawableView::drawView(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_roundedRectangle != nullptr) {
        target.draw(*m_roundedRectangle, states);
    }
}

