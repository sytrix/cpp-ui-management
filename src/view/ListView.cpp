#include <SFML/Graphics.hpp>
#include <iostream>
#include <typeinfo>

#include "ListView.hpp"


using namespace std;

ListView::ListView()
:   
    DrawableView::DrawableView(),
    m_horizontalAlignement(Alignement::Left),
    m_verticalAlignement(Alignement::Top)
{
    

}

ListView::~ListView()
{
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        delete *it;
    }
    m_listView.clear();
}

void ListView::addSubview(DrawableView *view)
{
    m_listView.push_back(view);
}

void ListView::setHorizontalAlignement(Alignement alignement)
{
    m_horizontalAlignement = alignement;
}

void ListView::setVerticalAlignement(Alignement alignement)
{
    m_verticalAlignement = alignement;
}

void ListView::fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView)
{
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        view->fillListWithInteractableView(listInteractableView);
    }
}

bool ListView::checkIfNeedPreRender()
{
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        if (view->checkIfNeedPreRender()) {
            return true;
        }
    }

    return DrawableView::checkIfNeedPreRender();
}

void ListView::preRender()
{
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        view->preRender();
    }

    DrawableView::preRender();
}

sf::Vector2f ListView::layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize)
{
    float originX;
    float originY;

    if (m_horizontalAlignement == Alignement::Left) {
        originX = topLeftCorner.x + m_paddingTopLeft.x;
    } else if (m_horizontalAlignement == Alignement::Center) {
        originX = topLeftCorner.x + m_drawableSize.x / 2 - m_maxWidth / 2;
    } else if (m_horizontalAlignement == Alignement::Right) {
        originX = topLeftCorner.x + m_drawableSize.x - m_maxWidth - m_paddingBottomRight.x;
    } 

    if (m_verticalAlignement == Alignement::Top) {
        originY = topLeftCorner.y + m_paddingTopLeft.y;
    } else if (m_verticalAlignement == Alignement::Center) {
        originY = topLeftCorner.y + m_drawableSize.y / 2 - m_maxHeight / 2;
    } else if (m_verticalAlignement == Alignement::Bottom) {
        originY = topLeftCorner.y + m_drawableSize.y - m_maxHeight - m_paddingBottomRight.y;
    } 

    sf::Vector2f childShift(0, 0);
    int index = 0;
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        view->layoutPosition(sf::Vector2f(originX + childShift.x + view->m_marginTopLeft.x, originY + childShift.y + view->m_marginTopLeft.y), m_drawableSize);
        childShift = this->nextPositionShift(childShift, view->m_drawableSize, index);
        index++;
    }

    return DrawableView::layoutPosition(topLeftCorner, parentSize);
}

sf::Vector2f ListView::nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index) 
{
    return sf::Vector2f(0, 0);
}

sf::Vector2f ListView::layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode)
{
    m_maxWidth = 0;
    m_maxHeight = 0;

    sf::Vector2f childShift(0, 0);
    int index = 0;
    for (std::vector<DrawableView*>::iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        sf::Vector2f childSize = view->layoutSize(
            sf::Vector2f(
                sizeToFit.x - childShift.x - m_paddingTopLeft.x - m_paddingBottomRight.x - view->m_marginTopLeft.x - view->m_marginBottomRight.x , 
                sizeToFit.y - childShift.y - m_paddingTopLeft.y - m_paddingBottomRight.y - view->m_marginTopLeft.y - view->m_marginBottomRight.y), 
            m_horizontalDisplayMode, m_verticalDisplayMode);

        if (m_maxWidth < childShift.x + childSize.x) {
            m_maxWidth = childShift.x + childSize.x;
        } 
        
        if (m_maxHeight < childShift.y + childSize.y) {
            m_maxHeight = childShift.y + childSize.y;
        }

        childShift = this->nextPositionShift(childShift, view->m_drawableSize, index);
        index++;
    }

    return DrawableView::layoutSize(sizeToFit, parentHorizontalDisplayMode, parentVerticalDisplayMode);
}

float ListView::maxWidthChild() 
{
    return m_maxWidth;
}

float ListView::maxHeightChild(float width) 
{
    return m_maxHeight;
}

void ListView::drawView(sf::RenderTarget &target, sf::RenderStates states) const
{
    DrawableView::drawView(target, states);

    for (std::vector<DrawableView*>::const_iterator it = m_listView.begin(); it != m_listView.end(); ++it) {
        DrawableView *view = *it;
        target.draw(*view, states);
    }
}
