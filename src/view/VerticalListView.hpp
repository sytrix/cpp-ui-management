#ifndef DEF_VERTICALLISTVIEW_H
#define DEF_VERTICALLISTVIEW_H

#include "ListView.hpp"

class VerticalListView : public ListView
{
    public:

        VerticalListView(float interSpace);

    protected:

        virtual sf::Vector2f nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index);
    
    private:

        float m_interSpace;



};

#endif

