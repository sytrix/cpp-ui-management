#include "ButtonLabelView.hpp"
#include "LabelView.hpp"
#include "ButtonView.hpp"


ButtonLabelView::ButtonLabelView(const String& string, const Font& font, unsigned int characterSize)
:
    LabelView(string, font, characterSize),
    ButtonView(),
    m_labelDefaultColor(sf::Color::White),
    m_labelOverColor(sf::Color::White)
{

}

void ButtonLabelView::setButtonLabelDefaultColor(Color color)
{
    LabelView::getLabel().setFillColor(color);
    m_labelDefaultColor = color;
}

void ButtonLabelView::setButtonLabelOverColor(Color color)
{
    m_labelOverColor = color;
}

void ButtonLabelView::fillListWithInteractableView(std::vector<DrawableView *> &listInteractableView)
{
    listInteractableView.push_back(this);
}

sf::Vector2f ButtonLabelView::layoutPosition(sf::Vector2f topLeftCorner, sf::Vector2f parentSize)
{
    
    LabelView::layoutPosition(topLeftCorner, m_drawableSize);
    return ButtonView::layoutPosition(topLeftCorner, m_drawableSize);
}

sf::Vector2f ButtonLabelView::layoutSize(sf::Vector2f sizeToFit, DisplayMode parentHorizontalDisplayMode, DisplayMode parentVerticalDisplayMode)
{
    LabelView::layoutSize(sizeToFit, parentHorizontalDisplayMode, parentVerticalDisplayMode);
    return ButtonView::layoutSize(sizeToFit, parentHorizontalDisplayMode, parentVerticalDisplayMode);
}

void ButtonLabelView::setDrawableFrame(float x, float y, float width, float height)
{
    ButtonView::setDrawableFrame(x, y, width, height);
    LabelView::setDrawableFrame(x, y, width, height);
}

float ButtonLabelView::maxWidthChild() 
{
    return LabelView::maxWidthChild();
}

float ButtonLabelView::maxHeightChild(float width) 
{
    return LabelView::maxHeightChild(width);
}

void ButtonLabelView::setHorizontalAlignement(Alignement alignement)
{
    LabelView::setHorizontalAlignement((Alignement)alignement);
}

void ButtonLabelView::setVerticalAlignement(Alignement alignement)
{
    LabelView::setVerticalAlignement(alignement);
}

bool ButtonLabelView::checkIfNeedPreRender()
{
    return ButtonView::checkIfNeedPreRender();
}

void ButtonLabelView::drawView(RenderTarget &target, RenderStates states) const
{
    LabelView::drawView(target, states);
}
