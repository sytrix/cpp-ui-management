#include "VerticalListView.hpp"

VerticalListView::VerticalListView(float interSpace)
:
    ListView::ListView(),
    m_interSpace(interSpace)
{

}

sf::Vector2f VerticalListView::nextPositionShift(sf::Vector2f previousChildShift, sf::Vector2f previousChildSize, int index) 
{
    return sf::Vector2f(0, previousChildShift.y + previousChildSize.y + m_interSpace);
}

