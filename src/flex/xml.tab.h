#ifndef XML_TAB_H
#define XML_TAB_H

#include "xml/XmlElement.h"

xml_element_array *parseXmlFile(const char *filepath);
void freeXmlStruct(xml_element_array *rootElementArray);

#endif