%{
    #include <stdio.h>
    #include <string.h>
    #include <stdlib.h>
    #include <math.h>
    #include <stdarg.h>
    #include "xml.tab.c"
    

    //#define PRINTF_DEBUG

    void debugprintf(const char *format, ...);
%}

%%

[ \t\n]+                        { /* ignore */ }
"<"                             { debugprintf("[open]"); return OPEN; }
">"                             { debugprintf("[close]"); return CLOSE; }
\"[^\"]*\"                      { yylval.sval = strdup(yytext); debugprintf("[string:%s]", yytext); return STRING; }
[a-zA-Z][a-zA-Z0-9\-]*          { yylval.sval = strdup(yytext); debugprintf("[name:%s]", yytext); return NAME; }
[0-9]+	                        { yylval.ival = atoi(yytext); debugprintf("[number:%d]", yylval); /*return(ENTIER);*/ }
[=/]                            { return yytext[0]; }
.                               { debugprintf("[invalidchar:%c]", yytext[0]); }
<<EOF>>                         { yyterminate(); return 0; }

%%

void debugprintf(const char *format, ...)
{
#ifdef PRINTF_DEBUG
    va_list ap;
    va_start(ap, format);
    vprintf(format, ap);
    va_end(ap);
#endif
}

int yywrap() {
    return 1;
}
