%{
  #include <stdio.h>
  #include <stdlib.h>

  #include "../../src/flex/xml/XmlElement.h"
  #include "../../src/flex/xml/XmlParam.h"

  // Declare stuff from Flex that Bison needs to know about:
  extern int yylex();
 
  int yyerror(const char *s);

  xml_element_array *rootElementArray = NULL;

%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  Initially (by default), yystype
// is merely a typedef of "int", but for non-trivial projects, tokens could
// be of any arbitrary data type.  So, to deal with that, the idea is to
// override yystype's default typedef to be a C union instead.  Unions can
// hold all of the types of tokens that Flex could return, and this this means
// we can return ints or floats or strings cleanly.  Bison implements this
// mechanism with the %union directive:
%union {
  int ival;
  float fval;
  char *sval;
  xml_param *param;
  xml_param_list *listParam;
  xml_element *element;
  xml_element_list *listElement;
}

// Define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the %union:
%token OPEN
%token CLOSE
%token <sval> STRING
%token <sval> NAME

%type <listParam> LIST_PARAMS
%type <listElement> LIST_ELEMENT
%type <element> ELEMENT

%start START

%%

START : LIST_ELEMENT        
  { 
    xml_element_list *rootElement = $1; 
    rootElementArray = xml_element_array_convert_list(rootElement);
  }
  ;

LIST_ELEMENT : LIST_ELEMENT ELEMENT     { 
                                          xml_element_list_add($1, $2);
                                          $$ = $1;
                                        }
  | ELEMENT                             {
                                          xml_element_list *list = xml_element_list_create();
                                          xml_element_list_add(list, $1);
                                          $$ = list;
                                        }
  ;

ELEMENT : OPEN NAME '/' CLOSE       { 
                                      $$ = xml_element_create($2, NULL, NULL); 
                                    }
  | OPEN NAME LIST_PARAMS '/' CLOSE { 
                                      xml_param_array *arrayParam = xml_param_array_convert_list($3);
                                      $$ = xml_element_create($2, arrayParam, NULL); 
                                    }
  | OPEN NAME CLOSE LIST_ELEMENT OPEN '/' NAME CLOSE 
  { 
    xml_element_array *arrayElement = xml_element_array_convert_list($4);
    $$ = xml_element_create($2, NULL, arrayElement); 
  }
  | OPEN NAME LIST_PARAMS CLOSE LIST_ELEMENT OPEN '/' NAME CLOSE
  { 
    xml_param_array *arrayParam = xml_param_array_convert_list($3);
    xml_element_array *arrayElement = xml_element_array_convert_list($5);
    $$ = xml_element_create($2, arrayParam, arrayElement); 
  }
  ;



LIST_PARAMS : LIST_PARAMS NAME '=' STRING      
                        { 
                          xml_param_list_put($1, xml_param_create($2, $4)); 
                          free($4);
                          $$ = $1;
                        }
  | NAME '=' STRING     { 
                          xml_param_list *list = xml_param_list_create(); 
                          xml_param_list_put(list, xml_param_create($1, $3)); 
                          free($3);
                          $$ = list;
                        }
  ;

%%


extern FILE *yyin;

xml_element_array *parseXmlFile(const char *filepath)
{
    if ((yyin = fopen(filepath,"r")) == NULL)
    {
        fprintf(stderr,"File not found or not readable.\n");
        exit(1);
    }

    // Start the parser
    yyparse();

#ifdef DEBUG
    char *xmlStr = xml_element_array_print(rootElementArray); 
    printf(xmlStr);
    free(xmlStr);
#endif

  return rootElementArray;
}

void freeXmlStruct(xml_element_array *rootElementArray) {
    xml_element_array_free(rootElementArray); 
    rootElementArray = NULL;
}

int yyerror(const char *s) {
    printf("[XML] %s", s);
    return 0;
}