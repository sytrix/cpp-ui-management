#ifndef XML_PARAM_H
#define XML_PARAM_H

typedef struct xml_param xml_param;
typedef struct xml_param_node xml_param_node;
typedef struct xml_param_list xml_param_list;
typedef struct xml_param_array xml_param_array;

struct xml_param {
    char *key;
    char *value;
};

struct xml_param_node {
    xml_param_node *next;
    xml_param *value;
} ;

struct xml_param_list {
    xml_param_node *first;
    unsigned int len;
};

struct xml_param_array {
    xml_param **array;
    unsigned int len;
};

/* ========== XML PARAM ========== */
xml_param *xml_param_create(char *key, char *value);
char *xml_param_print(xml_param *self);
void xml_param_free(xml_param *self);

/* ========== XML PARAM NODE ========== */
xml_param_node *xml_param_node_create(xml_param *value);
void xml_param_node_free_container(xml_param_node *self);
void xml_param_node_free(xml_param_node *self);

/* ========== XML PARAM LIST ========== */
xml_param_list *xml_param_list_create();
void xml_param_list_put(xml_param_list *self, xml_param *param);
void xml_param_list_free_container(xml_param_list *self);
void xml_param_list_free(xml_param_list *self);

/* ========== XML PARAM ARRAY ========== */
xml_param_array *xml_param_array_convert_list(xml_param_list *list);
int xml_param_array_len(xml_param_array *self);
xml_param *xml_param_array_get_index(xml_param_array *self, unsigned int index);
char *xml_param_array_print(xml_param_array *self);
void xml_param_array_free(xml_param_array *self);

#endif


