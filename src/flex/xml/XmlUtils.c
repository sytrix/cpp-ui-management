#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

unsigned long xml_string_hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++)) {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}


char *strjoin(const char *start, char **str, int nbStr, const char *delimiter, const char *end)
{
    int startLen = strlen(start);
    int *strLen = (int*)malloc(sizeof(int) * nbStr);
    int delimiterLen = strlen(delimiter);

    int count = startLen + strlen(end);

    for(int i = 0; i < nbStr; ++i) {
        int len = strlen(str[i]);
        strLen[i] = len;
        count += len;
    }

    if(nbStr > 2) {
        count += (nbStr - 1) * delimiterLen;
    }

    char *result = (char*)malloc(sizeof(char) * count);
    char *curs = result;

    strcpy(curs, start);
    curs += startLen;

    for(int i = 0; i < nbStr; i++) {
        if (i > 0) {
            strcpy(curs, delimiter);
            curs += delimiterLen;
        }
        strcpy(curs, str[i]);
        curs += strLen[i];
    }

    strcpy(curs, end);

    return result;
}

char *strformat(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    size_t needed = vsnprintf(NULL, 0, format, ap) + 1;
    char *buffer = (char*)malloc(needed);
    vsprintf(buffer, format, ap);
    va_end(ap);

    return buffer;
}