#include <stdlib.h>
#include "XmlElement.h"
#include "XmlUtils.h"
#include "XmlConstant.h"

/* ========== XML ELEMENT ========== */

xml_element *xml_element_create(char *name, xml_param_array *params, xml_element_array *childs)
{
    xml_element *self = (xml_element*)malloc(sizeof(xml_element));
    self->name = name;
    self->params = params;
    self->childs = childs;

    return self;
}

char *xml_element_print(xml_element *self)
{
    char *strParams = NULL;
    char *strElements = NULL;

    if (self->params) {
        strParams = xml_param_array_print(self->params);
    } 
    if (self->childs) {
        strElements = xml_element_array_print(self->childs);
    } 
    
    char *result = NULL;
    
    if (strParams) {
        if(strElements) {
            result = strformat("<%s %s>%s</%s>", self->name, strParams, strElements, self->name);
        } else {
            result = strformat("<%s %s/>", self->name, strParams);
        }
    } else {
        if(strElements) {
            result = strformat("<%s>%s</%s>", self->name, strElements, self->name);
        } else {
            result = strformat("<%s/>", self->name);
        }
    }

    free(strParams);
    free(strElements);

    return result;
}

void xml_element_free(xml_element *self) {
    PRINT_FUNCTION_FREE();

    free(self->name); self->name = NULL;
    
    if (self->params) {
        xml_param_array_free(self->params);
    }
    if (self->childs) {
        xml_element_array_free(self->childs);
    }
}

/* ========== XML ELEMENT NODE ========== */

xml_element_node *xml_element_node_create(xml_element *element) 
{
    xml_element_node *self = (xml_element_node*)malloc(sizeof(xml_element_node));
    self->next = NULL;
    self->value = element;

    return self;
}

void xml_element_node_free_container(xml_element_node *self)
{
    PRINT_FUNCTION_FREE_CONTAINER();

    if (self->next != NULL) {
        xml_element_node_free_container(self->next); self->next = NULL;
    }
    self->value = NULL;
    free(self);
}

void xml_element_node_free(xml_element_node *self)
{
    PRINT_FUNCTION_FREE();

    if (self->next != NULL) {
        xml_element_node_free(self->next); self->next = NULL;
    }
    xml_element_free(self->value); self->value = NULL;
    free(self);
}

/* ========== XML ELEMENT LIST ========== */

xml_element_list *xml_element_list_create() 
{
    xml_element_list *self = (xml_element_list*)malloc(sizeof(xml_element_list));
    self->first = NULL;
    self->last = NULL;
    self->len = 0; 

    return self;
}

void xml_element_list_add(xml_element_list *self, xml_element *element) 
{
    xml_element_node *node = (xml_element_node*)xml_element_node_create(element);

    if(self->first == NULL) {
        self->first = node;
        self->last = node;
    } else {
        self->last->next = node;
        self->last = node;
    }
    self->len++;
}

void xml_element_list_free_container(xml_element_list *self) 
{
    PRINT_FUNCTION_FREE_CONTAINER();

    xml_element_node_free_container(self->first); self->first = NULL;
    self->last = NULL;
    self->len = 0;
    free(self);
}

void xml_element_list_free(xml_element_list *self) 
{
    PRINT_FUNCTION_FREE();

    xml_element_node_free(self->first); self->first = NULL;
    self->last = NULL;
    self->len = 0;
    free(self);
}

/* ========== XML ELEMENT ARRAY ========== */

xml_element_array *xml_element_array_convert_list(xml_element_list *list) 
{
    xml_element_array *self = (xml_element_array*)malloc(sizeof(xml_element_array));
    self->array = (xml_element**)malloc(sizeof(xml_element) * list->len);
    self->len = list->len; 

    unsigned int i = 0;
    if (list->first != NULL) {
        xml_element_node *node = list->first;
        do {
            self->array[i] = node->value;
            node = node->next;
            i++;
        } while(node != NULL);
        
    }

    xml_element_list_free_container(list);

    return self;
}

int xml_element_array_len(xml_element_array *self) 
{
    return self->len;
}

xml_element *xml_element_array_get_index(xml_element_array *self, unsigned int index) 
{
    return self->array[index];
}

char *xml_element_array_print(xml_element_array *self) 
{
    
    int nbStr = self->len;
    char **str = (char**)malloc(sizeof(char *) * nbStr);

    for(int i = 0; i < nbStr; i++) {
        str[i] = xml_element_print(self->array[i]);
    }

    char *result = strjoin("", str, nbStr, " ", "");
    
    for(int i = 0; i < nbStr; i++) {
        free(str[i]);
    }
    free(str);

    return result;
}

void xml_element_array_free(xml_element_array *self) 
{
    PRINT_FUNCTION_FREE();

    for(unsigned int i = 0; i < self->len; i++) {
        xml_element *element = self->array[i];
        xml_element_free(element); self->array[i] = NULL;
    }
    free(self->array); self->array = NULL;
    self->len = 0;
    free(self);
}


