#ifndef XML_UTILS_H
#define XML_UTILS_H

unsigned long xml_string_hash(unsigned char *str);

char *strjoin(const char *start, char **str, int nbStr, const char *delimiter, const char *end);
char *strformat(const char *format, ...);

#endif