

#ifdef DEBUG
    #include <stdio.h>
    #define PRINT_FUNCTION(name) printf("[XML_DEBUG][%s] %s::%s:%d\n", name, __FILE__, __FUNCTION__, __LINE__);
#endif


#if DEBUG && DEBUG_STACK_TRACE_FREE_CONTAINER
    #define PRINT_FUNCTION_FREE_CONTAINER() PRINT_FUNCTION("FREE_CONTAINER");
#else
    #define PRINT_FUNCTION_FREE_CONTAINER();
#endif

#if DEBUG && DEBUG_STACK_TRACE_FREE
    #define PRINT_FUNCTION_FREE() PRINT_FUNCTION("FREE");
#else
    #define PRINT_FUNCTION_FREE();
#endif
