#include <stdlib.h>
#include <string.h>

#include "XmlParam.h"
#include "XmlUtils.h"
#include "XmlConstant.h"


/* ========== XML PARAM ========== */

char *valueContent(const char *value) 
{
    int contentLenght = strlen(value) - 2;
    char *result = (char*)malloc(sizeof(char) * (contentLenght + 1));

    for (int i = 0; i < contentLenght; i++) {
        result[i] = value[i + 1];
    }

    result[contentLenght] = '\0';

    return result;
}

xml_param *xml_param_create(char *key, char *value)
{
    xml_param *self = (xml_param*)malloc(sizeof(xml_param));
    self->key = key;
    self->value = valueContent(value);

    return self;
}

char *xml_param_print(xml_param *self)
{
    return strformat("%s=\"%s\"", self->key, self->value);
}

void xml_param_free(xml_param *self)
{
    PRINT_FUNCTION_FREE();

    free(self->key); self->key = NULL;
    free(self->value); self->value = NULL;
    free(self);
}

/* ========== XML PARAM NODE ========== */

xml_param_node *xml_param_node_create(xml_param *value)
{
    xml_param_node *self = (xml_param_node*)malloc(sizeof(xml_param_node));
    self->next = NULL;
    self->value = value;

    return self;
}

void xml_param_node_free_container(xml_param_node *self)
{
    PRINT_FUNCTION_FREE_CONTAINER();

    if (self->next != NULL) {
        xml_param_node_free_container(self->next); self->next = NULL;
    }
    self->value = NULL;
    free(self);
}

void xml_param_node_free(xml_param_node *self)
{
    PRINT_FUNCTION_FREE();

    if (self->next != NULL) {
        xml_param_node_free(self->next); self->next = NULL;
    }
    xml_param_free(self->value); self->value = NULL;
    free(self);
}

/* ========== XML PARAM LIST ========== */

xml_param_list *xml_param_list_create()
{
    xml_param_list *self = (xml_param_list*)malloc(sizeof(xml_param_list));
    self->first = NULL;
    self->len = 0;

    return self;
}

void xml_param_list_put(xml_param_list *self, xml_param *param)
{
    if (self->first == NULL) {
        self->first = xml_param_node_create(param);
        self->len++;
    } else {
        xml_param_node *node = self->first;
        xml_param_node *previousNode = NULL;
        do {
            if (strcmp(node->value->key, param->key) == 0) {
                free(node->value->value);
                node->value->value = strdup(param->value);
                xml_param_free(param);
                return;
            }
            previousNode = node;
            node = node->next;
        } while(node != NULL);

        previousNode->next = xml_param_node_create(param);
        self->len++;
    }
}

void xml_param_list_free_container(xml_param_list *self)
{
    PRINT_FUNCTION_FREE_CONTAINER();

    xml_param_node_free_container(self->first); self->first = NULL;
    self->len = 0;
    free(self);
}

void xml_param_list_free(xml_param_list *self)
{
    PRINT_FUNCTION_FREE();

    xml_param_node_free(self->first); self->first = NULL;
    self->len = 0;
    free(self);
}


/* ========== XML PARAM ARRAY ========== */

xml_param_array *xml_param_array_convert_list(xml_param_list *list) 
{
    xml_param_array *self = (xml_param_array*)malloc(sizeof(xml_param_array));
    self->array = (xml_param**)malloc(sizeof(xml_param) * list->len);
    self->len = list->len;

    unsigned int i = 0;
    if (list->first != NULL) {
        xml_param_node *node = list->first;
        do {
            self->array[i] = node->value;
            node = node->next;
            i++;
        } while(node != NULL);
        
    }

    xml_param_list_free_container(list);
    
    return self;
}

int xml_param_array_len(xml_param_array *self) {
    return self->len;
}

xml_param *xml_param_array_get_index(xml_param_array *self, unsigned int index) {
    return self->array[index];
}


char *xml_param_array_print(xml_param_array *self) {
    
    int nbStr = self->len;
    char **str = (char **)malloc(sizeof(char *) * nbStr);

    for(int i = 0; i < nbStr; i++) {
        str[i] = xml_param_print(self->array[i]);
    }

    char *result = strjoin("", str, nbStr, " ", "");
    
    for(int i = 0; i < nbStr; i++) {
        free(str[i]); str[i] = NULL;
    }
    free(str);

    return result;
}


void xml_param_array_free(xml_param_array *self) {
    
    PRINT_FUNCTION_FREE();

    for(unsigned int i = 0; i < self->len; i++) {
        xml_param_free(self->array[i]); self->array[i] = NULL;
    }
    free(self->array); self->array = NULL;
    self->len = 0;
}
