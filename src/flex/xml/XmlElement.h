#ifndef XML_ELEMENT_H
#define XML_ELEMENT_H

#include "XmlParam.h"

typedef struct xml_element xml_element;
typedef struct xml_element_node xml_element_node;
typedef struct xml_element_list xml_element_list;
typedef struct xml_element_array xml_element_array;

struct xml_element {
    xml_element_array *childs;
    xml_param_array *params;
    char *name;
};

struct xml_element_node {
    xml_element_node *next;
    xml_element *value;
};

struct xml_element_list {
    xml_element_node *first;
    xml_element_node *last;
    unsigned int len;
};

struct xml_element_array {
    xml_element **array;
    unsigned int len;
};


/* ========== XML ELEMENT ========== */
xml_element *xml_element_create(char *name, xml_param_array *params, xml_element_array *childs);
char *xml_element_print(xml_element *self);
void xml_element_free(xml_element *self);

/* ========== XML ELEMENT NODE ========== */
xml_element_node *xml_element_node_create(xml_element *element);
void xml_element_node_free_container(xml_element_node *self);
void xml_element_node_free(xml_element_node *self);

/* ========== XML ELEMENT LIST ========== */
xml_element_list *xml_element_list_create();
void xml_element_list_add(xml_element_list *self, xml_element *element);
void xml_element_list_free_container(xml_element_list *self);
void xml_element_list_free(xml_element_list *self);

/* ========== XML ELEMENT ARRAY ========== */
xml_element_array *xml_element_array_convert_list(xml_element_list *list);
int xml_element_array_len(xml_element_array *self);
xml_element *xml_element_array_get_index(xml_element_array *self, unsigned int index);
char *xml_element_array_print(xml_element_array *self);
void xml_element_array_free(xml_element_array *self);

#endif