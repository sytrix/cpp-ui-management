#include "flex/xml.tab.h"

#include <stdio.h>

int main(int argc, char *argv[])
{
    xml_element_array *root = parseXmlFile("res/test.xml");
    freeXmlStruct(root);

    return 0;
}

