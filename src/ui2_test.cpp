#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include "view/DrawableView.hpp"
#include "view/LabelView.hpp"
#include "view/HorizontalListView.hpp"
#include "view/VerticalListView.hpp"
#include "view/ButtonView.hpp"
#include "view/ListView.hpp"
#include "view/ButtonLabelView.hpp"
#include "view/ImageView.hpp"


#include "drawable/HorizontalScrollBar.hpp"
#include "drawable/VerticalScrollBar.hpp"

using namespace sf;

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 900

int main()
{
    // Create the main window
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "SFML window");
    
    sf::Font fontTitle;
    if (!fontTitle.loadFromFile("res/font/theboldfont.ttf"))
        return EXIT_FAILURE;

    sf::Font fontText;
    if (!fontText.loadFromFile("res/font/LANENAR_.ttf"))
        return EXIT_FAILURE;

    // Load a sprite to display
    sf::Texture texture;
    if (!texture.loadFromFile("res/image/cute.png"))
        return EXIT_FAILURE;
    sf::Sprite sprite(texture);
    sprite.setScale(0.2f, 0.2f);
    
    LabelView *labelView = new LabelView("Hello World", fontTitle, 50);
    labelView->setViewOutlineColor(sf::Color(255, 255, 255));
    labelView->setViewOutlineThickness(-2.f);
    labelView->setViewSize(0, 80);
    labelView->setVerticalDisplayMode(DisplayMode::Fix);
    labelView->setHorizontalAlignement(Alignement::Center);
    labelView->setVerticalAlignement(Alignement::Center);
    labelView->setViewPadding(15, 5);

    sf::String str = L"En 2015, il propose (sous la marque Tesla) un système dit « Powerwall » de stockage tampon d'énergie domestique intermittente (ex : solaire et/ou éolienne) via des batteries lithium-ion présentant une bonne efficacité énergétique et bonne durée de vie, acceptant des recharges incomplètes tout en fournissant une tension constante durant toute leur décharge26. Ces powerwalls pourront équiper des immeubles et des maisons et, selon Elon Musk, pourraient contribuer à « transformer totalement l'infrastructure énergétique mondiale pour la rendre totalement durable et sans produire d'émissions de carbone ». Chaque bloc accumulateur mesure 130 × 86 × 18 cm et peut accumuler 7 ou 10 kWh, pour une puissance continue de 2 kW (pic à 3,3 kW). Le système peut aussi accumuler de l'énergie achetée en heures creuses ou contribuer à la régulation de fréquence du réseau.";
    LabelView *textLabel = new LabelView(str, fontText, 18);
    textLabel->setViewBackgroundColor(sf::Color::Black);
    textLabel->setViewPadding(20, 20);
    textLabel->setViewOutlineColor(sf::Color(64, 64, 64));
    textLabel->setHorizontalDisplayMode(DisplayMode::FitToSuperview);
    textLabel->setVerticalDisplayMode(DisplayMode::FitToChildrens);
    textLabel->setViewOutlineThickness(-1.f);
    //textLabel->setViewBackgroundColor(sf::Color(50, 110, 220));
    textLabel->getLabel().setFillColor(sf::Color(255, 255, 255));
    textLabel->setViewSize(400, 450.f);
    textLabel->setVerticalAlignement(Alignement::Top);
    textLabel->setHorizontalAlignement(Alignement::Left);
    textLabel->setViewMargin(sf::Vector2f(), sf::Vector2f(0, 60));

    ImageView *imageView = new ImageView();
    imageView->setImageViewTexture(texture);
    imageView->setHorizontalDisplayMode(DisplayMode::Fix);
    imageView->setVerticalDisplayMode(DisplayMode::Fix);
    imageView->setViewWidth(200.f);
    imageView->setViewHeight(300.f);
    imageView->setViewCornerRadius(50.f);
    imageView->setViewOutlineColor(sf::Color(255, 0, 0));
    imageView->setViewOutlineThickness(2.f);
    imageView->setImageTintColor(sf::Color(255, 200, 255));

    HorizontalListView *imageTextHorizontalListView = new HorizontalListView(20);
    imageTextHorizontalListView->addSubview(imageView);
    imageTextHorizontalListView->addSubview(textLabel);
    imageTextHorizontalListView->setViewSize(800, 80.f);
    imageTextHorizontalListView->setHorizontalDisplayMode(DisplayMode::FitToSuperview);
    imageTextHorizontalListView->setVerticalDisplayMode(DisplayMode::FitToChildrens);
    imageTextHorizontalListView->setHorizontalAlignement(Alignement::Left);
    //imageTextHorizontalListView->setVerticalAlignement(HorizontalListView::Up);
    //imageTextHorizontalListView->setViewAllowOverflow(false);


    VerticalListView *verticalListView = new VerticalListView(20);
    verticalListView->addSubview(labelView);
    verticalListView->addSubview(imageTextHorizontalListView);
    verticalListView->setHorizontalDisplayMode(DisplayMode::FitToSuperview);
    verticalListView->setVerticalDisplayMode(DisplayMode::FitToSuperview);
    verticalListView->setViewPadding(20, 20);

    ButtonLabelView *buttonView = new ButtonLabelView("Next", fontTitle, 26);
    buttonView->setButtonLabelDefaultColor(sf::Color(16, 16, 16));
    buttonView->setButtonLabelOverColor(sf::Color(16, 16, 16));
    buttonView->setButtonDefaultColor(sf::Color(53, 140, 214));
    buttonView->setButtonOverColor(sf::Color(140, 214, 53));
    buttonView->setViewSize(170, 40);
    buttonView->setHorizontalDisplayMode(DisplayMode::Fix);
    buttonView->setVerticalDisplayMode(DisplayMode::Fix);
    buttonView->setViewPadding(sf::Vector2f(30, 5));
    buttonView->setVerticalAlignement(Alignement::Center);
    buttonView->setHorizontalAlignement(Alignement::Center);

    ButtonLabelView *buttonView2 = new ButtonLabelView(L"Previous", fontTitle, 26);
    buttonView2->setButtonLabelDefaultColor(sf::Color(16, 16, 16));
    buttonView2->setButtonLabelOverColor(sf::Color(16, 16, 16));
    buttonView2->setButtonDefaultColor(sf::Color(53, 140, 214));
    buttonView2->setButtonOverColor(sf::Color(140, 214, 53));
    buttonView2->setViewSize(200, 40);
    buttonView2->setHorizontalDisplayMode(DisplayMode::Fix);
    buttonView2->setVerticalDisplayMode(DisplayMode::Fix);
    buttonView2->setViewPadding(sf::Vector2f(30, 5));
    buttonView2->setVerticalAlignement(Alignement::Center);
    buttonView2->setHorizontalAlignement(Alignement::Center);


    LabelView *buttonView3 = new LabelView(L"Action", fontTitle, 26);
    buttonView3->getLabel().setFillColor(sf::Color(16, 16, 16));
    buttonView3->setViewSize(170, 40);
    buttonView3->setHorizontalDisplayMode(DisplayMode::Fix);
    buttonView3->setVerticalDisplayMode(DisplayMode::Fix);
    buttonView3->setViewPadding(sf::Vector2f(30, 5));
    buttonView3->setVerticalAlignement(Alignement::Center);
    buttonView3->setHorizontalAlignement(Alignement::Center);

    ButtonView *trueButtonView = new ButtonView();
    trueButtonView->setButtonDefaultColor(sf::Color(53, 140, 214));
    trueButtonView->setButtonOverColor(sf::Color(140, 214, 53));
    trueButtonView->addSubview(buttonView3);
    trueButtonView->setHorizontalDisplayMode(DisplayMode::FitToChildrens);
    trueButtonView->setVerticalDisplayMode(DisplayMode::FitToChildrens);

    HorizontalListView *horizontalListView = new HorizontalListView(10);
    horizontalListView->addSubview(buttonView);
    horizontalListView->addSubview(buttonView2);
    horizontalListView->addSubview(trueButtonView);
    horizontalListView->setViewSize(800, 80.f);
    horizontalListView->setHorizontalDisplayMode(DisplayMode::FitToSuperview);
    horizontalListView->setVerticalDisplayMode(DisplayMode::FitToSuperview);
    horizontalListView->setViewPadding(20, 20);
    horizontalListView->setHorizontalAlignement(Alignement::Center);
    horizontalListView->setVerticalAlignement(Alignement::Bottom);
    horizontalListView->setViewMargin(sf::Vector2f(220.f, 0.f), sf::Vector2f());
    //horizontalListView->setViewAllowOverflow(false);

    ListView listView;
    listView.addSubview(verticalListView);
    listView.addSubview(horizontalListView);
    listView.setHorizontalDisplayMode(DisplayMode::FitToSuperview);
    listView.setVerticalDisplayMode(DisplayMode::FitToSuperview);
    listView.setViewBackgroundColor(sf::Color(30, 30, 30));
    listView.setViewAllowOverflow(false);

    listView.layoutContent(sf::FloatRect(50, 50, WINDOW_WIDTH - 100, WINDOW_HEIGHT - 100));

    std::vector<DrawableView *> *interactableViews = listView.getListOfInteractableView();



    VerticalScrollBar horizontalScrollBar;
    horizontalScrollBar.setColor(sf::Color::White);
    horizontalScrollBar.setBarSize(900.f, 400.f);
    horizontalScrollBar.setPosition(50.f, 50.f);
    



    
    
    sf::Clock clock;
    float width = 400;
    sprite.setPosition(width + 20, sprite.getPosition().y);

    window.setFramerateLimit(60);
    int i = 0;
    float mouseX;
    float mouseY;
    bool haveToUpdateLayout = false;
    bool mousePressed = false;
    int editMode = 0;

    sf::Vector2f viewStartPoint(50, 50);
    sf::Vector2f viewEndPoint(WINDOW_WIDTH - 100, WINDOW_HEIGHT - 100);

    sf::Clock benchmarkClock;
    float benchmarkDrawTime = 0;
    float benchmarkPreRenderIfNeedTime = 0;
    float benchmarkLayoutDrawTime = 0;

    // Start the game loop
    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            for (std::vector<DrawableView *>::iterator it = interactableViews->begin(); it != interactableViews->end(); it++) {
                DrawableView *view = *it;
                view->updateEvent(event);
            }

            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed) {
                if(event.key.code == sf::Keyboard::Left) {
                    width -= 2;
                    //textLabel->fit(width, texture.getSize().y * 0.2f);
                    sprite.setPosition(width + 20, sprite.getPosition().y);
                }
                if(event.key.code == sf::Keyboard::Right) {
                    width += 2;
                    //textLabel->fit(width, -1);
                    sprite.setPosition(width + 20, sprite.getPosition().y);
                }
                if(event.key.code == sf::Keyboard::A) {
                    editMode = 0;
                }
                if(event.key.code == sf::Keyboard::Z) {
                    editMode = 1;
                }
                if(event.key.code == sf::Keyboard::E) {
                    editMode = 2;
                }
            }
            if (event.type == sf::Event::MouseMoved) {
                mouseX = event.mouseMove.x;
                mouseY = event.mouseMove.y;

                haveToUpdateLayout = true;
                //listView.eventMouseOver(mouseX, mouseY);
            }
            if (event.type == sf::Event::MouseButtonPressed) {
                mousePressed = true;
            }
            if (event.type == sf::Event::MouseButtonReleased) {
                mousePressed = false;
            }
            if (event.type == sf::Event::MouseWheelScrolled) {
                
            }
            if (event.type == sf::Event::MouseWheelMoved) {
                horizontalScrollBar.setOffset(horizontalScrollBar.getOffset() - event.mouseWheel.delta * 20);
            }
        }

        if(clock.getElapsedTime().asSeconds() > 1.f) {
            std::cout << "fps:" << i 
                << " - tDraw:" << benchmarkDrawTime 
                << " - tPreRender:" << benchmarkPreRenderIfNeedTime 
                << " - tLayout:" << benchmarkLayoutDrawTime 
                << " - total:" << (benchmarkDrawTime + benchmarkPreRenderIfNeedTime + benchmarkLayoutDrawTime) 
                << std::endl;
            clock.restart();
            i = 0;
            benchmarkDrawTime = 0.f;
            benchmarkPreRenderIfNeedTime = 0.f;
            benchmarkLayoutDrawTime = 0.f;
        }
        i++;

        if (mousePressed) {
            if (haveToUpdateLayout) {
                if (editMode == 0) {
                    viewEndPoint = sf::Vector2f(mouseX, mouseY);
                } else if (editMode == 1) {
                    viewStartPoint = sf::Vector2f(mouseX, mouseY);
                } else if (editMode == 2) {
                    viewEndPoint = sf::Vector2f(mouseX + viewEndPoint.x - viewStartPoint.x, mouseY + viewEndPoint.y - viewStartPoint.y);
                    viewStartPoint = sf::Vector2f(mouseX, mouseY);
                    
                    
                }
                benchmarkClock.restart();
                listView.layoutContent(sf::FloatRect(viewStartPoint.x, viewStartPoint.y, viewEndPoint.x - viewStartPoint.x, viewEndPoint.y - viewStartPoint.y));
                benchmarkLayoutDrawTime += benchmarkClock.getElapsedTime().asSeconds();
                haveToUpdateLayout = false;
            }
        }

        //listView.layoutContent(sf::FloatRect(50, 50, WINDOW_WIDTH * 0.5 + WINDOW_WIDTH * 0.5 * sin(0.2 * clock.getElapsedTime().asSeconds()), WINDOW_HEIGHT - 100));
        benchmarkClock.restart();
        listView.makePreRenderIfNeeded();
        benchmarkPreRenderIfNeedTime += benchmarkClock.getElapsedTime().asSeconds();

        // Clear screen
        window.clear(sf::Color(16, 16, 16));
        // Draw the sprite
        //window.draw(sprite);
        // Draw the string
        
        benchmarkClock.restart();
        window.draw(listView);
        benchmarkDrawTime += benchmarkClock.getElapsedTime().asSeconds();
        // Update the window

        window.draw(horizontalScrollBar);

        window.display();
    }
    return EXIT_SUCCESS;
}


