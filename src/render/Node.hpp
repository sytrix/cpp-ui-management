#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Node
{
	public:
		
		void generate(sf::Vector2u renderSize);
		bool isGenerate();
		sf::Texture *getTexture();

		virtual std::string getTypeString();

	protected:

		Node();
		~Node();

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);
		
	private:

		sf::Texture *m_texture;
		float m_generationTime;

};

#endif
