#ifndef NODEMASK_H
#define NODEMASK_H

#include "Node.hpp"

class NodeMask : public Node
{
	public:
		NodeMask();
		~NodeMask();

		void setInputNodeMask(Node *inputMask);
		void setInputNodeBack(Node *inputBack);
		void setInputNodeFront(Node *inputFront);
		
		static void destroyShader();

		virtual std::string getTypeString();
		
	protected:

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);
		
	private:
		
		Node *m_inputMask;
		Node *m_inputBack;
		Node *m_inputFront;
};

#endif
