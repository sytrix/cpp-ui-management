#include "NodeMix.hpp"



NodeMix::NodeMix() 
: 
    Node::Node(),
    m_inputA(nullptr),
    m_inputB(nullptr)
{
	
}

NodeMix::~NodeMix() {
	
}

void NodeMix::setInputNodeA(Node *inputA)
{
    m_inputA = inputA;
}

void NodeMix::setInputNodeB(Node *inputB)
{
    m_inputB = inputB;
}

std::string NodeMix::getTypeString()
{
    return "Mix";
}

sf::Texture *NodeMix::generateTexture(sf::Vector2u renderSize)
{
    if (!m_inputA || !m_inputB) {
        return nullptr;
    }

    if (!m_inputA->isGenerate()) {
        m_inputA->generate(renderSize);
    }

    if (!m_inputB->isGenerate()) {
        m_inputB->generate(renderSize);
    }

    sf::Sprite spriteA(*(m_inputA->getTexture()));
    sf::Sprite spriteB(*(m_inputB->getTexture()));
    spriteA.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));
    spriteB.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));

    sf::RenderTexture finalRender;
    finalRender.create(renderSize.x, renderSize.y);

    finalRender.draw(spriteA);
    finalRender.draw(spriteB, sf::BlendMultiply);

    return new sf::Texture(finalRender.getTexture());
}