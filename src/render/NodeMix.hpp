#ifndef NODEMIX_H
#define NODEMIX_H

#include "Node.hpp"

class NodeMix : public Node
{
	public:
		NodeMix();
		~NodeMix();

		void setInputNodeA(Node *inputA);
		void setInputNodeB(Node *inputB);
		
		virtual std::string getTypeString();
		
	protected:

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);
		
	private:
		
		Node *m_inputA;
		Node *m_inputB;
};

#endif
