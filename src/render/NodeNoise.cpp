#include "NodeNoise.hpp"

sf::Shader *NodeNoise::s_shaderNoise(nullptr);
sf::Shader *NodeNoise::s_shaderBicubic(nullptr);

NodeNoise::NodeNoise() 
: 
    Node::Node()
{
    if (!NodeNoise::s_shaderNoise) {
        NodeNoise::s_shaderNoise = new sf::Shader();
        if (!NodeNoise::s_shaderNoise->loadFromFile("res/shader/noise.frag", sf::Shader::Fragment)) {
            std::cerr << "shader load error " << std::endl;
        }
    }
	
    if (!NodeNoise::s_shaderBicubic) {
        NodeNoise::s_shaderBicubic = new sf::Shader();
        if (!NodeNoise::s_shaderBicubic->loadFromFile("res/shader/bicubic.frag", sf::Shader::Fragment)) {
            std::cerr << "shader load error " << std::endl;
        }
    }
}

NodeNoise::~NodeNoise() 
{
    for (std::vector<Layer *>::iterator it = m_layers.begin(); it != m_layers.end(); ++it) {
        delete *it;
    }
    m_layers.clear();
}

void NodeNoise::destroyShader()
{
    delete NodeNoise::s_shaderNoise; NodeNoise::s_shaderNoise = nullptr;
    delete NodeNoise::s_shaderBicubic; NodeNoise::s_shaderBicubic = nullptr;
}

std::string NodeNoise::getTypeString()
{
    return "Noise";
}

void NodeNoise::addLayer(float ratio, float amplitude, float seed)
{
    Layer *layer = new Layer;
    layer->ratio = ratio;
    layer->amplitude = amplitude;
    layer->seed = seed;

    m_layers.push_back(layer);
}

sf::Texture *NodeNoise::generateTexture(sf::Vector2u renderSize)
{
    sf::RenderTexture finalRender;
    finalRender.create(renderSize.x, renderSize.y);

    for (std::vector<Layer *>::iterator it = m_layers.begin(); it != m_layers.end(); ++it) {
        Layer *layer = *it;
        this->generateLayer(&finalRender, renderSize.x, renderSize.y, layer->ratio, layer->amplitude, layer->seed);
    }

    return new sf::Texture(finalRender.getTexture());
}

void NodeNoise::generateLayer(sf::RenderTexture *result, int renderWidth, int renderHeight, float ratio, float amplitude, float seed)
{
    int texW = renderWidth * ratio;
    int texH = renderHeight * ratio;

    NodeNoise::s_shaderNoise->setUniform("time", seed);
    sf::RenderTexture renderNoise;
    renderNoise.create(texW, texH);
    renderNoise.draw(sf::RectangleShape(sf::Vector2f(texW, texH)), NodeNoise::s_shaderNoise);


    NodeNoise::s_shaderBicubic->setUniform("texture", sf::Shader::CurrentTexture);
    NodeNoise::s_shaderBicubic->setUniform("textureSize", sf::Glsl::Vec2(texW, texH));
    NodeNoise::s_shaderBicubic->setUniform("amplitude", amplitude);
    sf::Texture textureOfNoise(renderNoise.getTexture());
    textureOfNoise.setRepeated(true);
    sf::Sprite spriteOfNoise(textureOfNoise);
    spriteOfNoise.setScale(1.f / ratio, 1.f / ratio);
    
    sf::RenderTexture renderBlurry;
    renderBlurry.create(renderWidth, renderHeight);
    renderBlurry.draw(spriteOfNoise, NodeNoise::s_shaderBicubic);

    result->draw(sf::Sprite(renderBlurry.getTexture()), sf::BlendAdd);
}
