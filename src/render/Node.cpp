#include "Node.hpp"



Node::Node()
: 
    m_texture(nullptr),
    m_generationTime(0.f)
{
	
}

Node::~Node() 
{
    delete m_texture; m_texture = nullptr;
}

void Node::generate(sf::Vector2u renderSize) 
{
    sf::Clock clockGenerationTime;

    m_texture = this->generateTexture(renderSize);

    m_generationTime = clockGenerationTime.getElapsedTime().asSeconds();
}

bool Node::isGenerate()
{
    if (m_texture) {
        return true;
    }
    return false;
}

sf::Texture *Node::getTexture()
{
    return m_texture;
}

std::string Node::getTypeString()
{
    return "Default";
}

sf::Texture *Node::generateTexture(sf::Vector2u renderSize)
{
    return nullptr;
}
