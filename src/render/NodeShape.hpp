#ifndef NODESHAPE_H
#define NODESHAPE_H

#include "Node.hpp"

enum Shape {UNDEFINED_SHAPE, CIRCLE_NORMAL_LAYER, RECTANGLE};

class NodeShape : public Node
{
	public:
		NodeShape();
		~NodeShape();

		void setShapeType(Shape shape);
		void setNbColumn(int nbColumn);
		void setNbLine(int nbLine);
		void setMarginRatio(float marginRatio);
		void setRotation(float rotation);

		virtual std::string getTypeString();
		
	protected:

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);

	private:

		void constructShape(sf::VertexArray &vertexArray, sf::Transform transform, sf::Vector2f size);

		Shape m_shapeType;
		int m_nbColumn;
		int m_nbLine;
		float m_marginRatio;
		float m_rotation;
};

#endif
