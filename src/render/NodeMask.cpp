#include "NodeMask.hpp"



NodeMask::NodeMask() 
: 
    Node::Node(),
    m_inputMask(nullptr),
    m_inputBack(nullptr),
    m_inputFront(nullptr)
{
	
}

NodeMask::~NodeMask() 
{
	
}

void NodeMask::setInputNodeMask(Node *inputMask)
{
    m_inputMask = inputMask;
}

void NodeMask::setInputNodeBack(Node *inputBack)
{
    m_inputBack = inputBack;
}

void NodeMask::setInputNodeFront(Node *inputFront)
{
    m_inputFront = inputFront;
}

std::string NodeMask::getTypeString()
{
    return "Mask";
}

sf::Texture *NodeMask::generateTexture(sf::Vector2u renderSize)
{
    if (!m_inputMask || !m_inputBack || !m_inputFront) {
        return nullptr;
    }

    if (!m_inputMask->isGenerate()) {
        m_inputMask->generate(renderSize);
    }

    if (!m_inputBack->isGenerate()) {
        m_inputBack->generate(renderSize);
    }

    if (!m_inputFront->isGenerate()) {
        m_inputFront->generate(renderSize);
    }

    sf::Sprite spriteMask(*(m_inputMask->getTexture()));
    spriteMask.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));

    sf::Sprite spriteBack(*(m_inputBack->getTexture()));
    spriteBack.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));

    sf::Sprite spriteFront(*(m_inputFront->getTexture()));
    spriteFront.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));
    
    
    sf::RenderTexture renderTextureMaskFront;
    renderTextureMaskFront.create(renderSize.x, renderSize.y);
    renderTextureMaskFront.clear(sf::Color::Transparent);
    renderTextureMaskFront.draw(spriteMask);
    renderTextureMaskFront.draw(spriteFront, sf::BlendMode(sf::BlendMode::Factor::DstColor, sf::BlendMode::Factor::Zero, sf::BlendMode::Equation::Add));

    sf::Sprite spriteResult(renderTextureMaskFront.getTexture());
    spriteResult.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));
    
    sf::RenderTexture renderTextureBack;
    renderTextureBack.create(renderSize.x, renderSize.y);
    renderTextureBack.draw(spriteMask);
    renderTextureBack.draw(spriteBack, sf::BlendMode(sf::BlendMode::Factor::Zero, sf::BlendMode::Factor::One, sf::BlendMode::Equation::Add));
    //renderTextureBack.draw(spriteResult, sf::BlendAdd);

    return new sf::Texture(renderTextureBack.getTexture());
}
