#ifndef NODECOLOR_H
#define NODECOLOR_H

#include "Node.hpp"

class ColorizationPoint
{
	public:
		float position;
		sf::Color color;
};

class NodeColor : public Node
{
	public:
		NodeColor();
		~NodeColor();

		static void destroyShader();
		
		virtual std::string getTypeString();

		void setInputNode(Node *inputNode);
		void resetColoriztaionPoints();
		void addColorizationPoint(float position, sf::Color color);
		
	protected:

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);
		
	private:
		
		static sf::Shader *s_shaderColorize;

		Node *m_inputNode;
		std::vector<ColorizationPoint *> m_points;


};

#endif
