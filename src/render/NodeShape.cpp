#include "NodeShape.hpp"

#include <cmath>



NodeShape::NodeShape() 
: 
    Node::Node(),
    m_shapeType(Shape::UNDEFINED_SHAPE),
    m_nbColumn(1),
    m_nbLine(1),
    m_marginRatio(0.f),
    m_rotation(0.f)
{
	
}

NodeShape::~NodeShape() {
	
}

void NodeShape::setShapeType(Shape shape)
{
    m_shapeType = shape;
}

void NodeShape::setNbColumn(int nbColumn)
{
    m_nbColumn = nbColumn;
}

void NodeShape::setNbLine(int nbLine)
{
    m_nbLine = nbLine;
}

void NodeShape::setMarginRatio(float marginRatio)
{
    m_marginRatio = marginRatio;
}

void NodeShape::setRotation(float rotation)
{
    m_rotation = rotation;
}

std::string NodeShape::getTypeString()
{
    return "Shape:" + std::to_string(m_shapeType);
}
		
sf::Texture *NodeShape::generateTexture(sf::Vector2u renderSize)
{
    sf::Vector2f tileSize(renderSize.x / (float)m_nbColumn, renderSize.y / (float)m_nbLine);
    sf::Vector2f shapeSize(tileSize.x * (1.f - m_marginRatio), tileSize.y * (1.f - m_marginRatio));
    float halfX = tileSize.x / 2.f;
    float halfY = tileSize.y / 2.f;


    sf::RenderTexture finalRender;
    finalRender.create(renderSize.x, renderSize.y);
    finalRender.clear();

    
    

    for (int y = 0; y < m_nbLine; ++y) {
        for (int x = 0; x < m_nbColumn; ++x) {
            sf::VertexArray vertexArray(sf::PrimitiveType::TrianglesFan, 0);
            sf::Transform transform;
            transform.translate(sf::Vector2f(tileSize.x * x + halfX, tileSize.y * y + halfY));
            transform.rotate(m_rotation);
            this->constructShape(vertexArray, transform, shapeSize);
            finalRender.draw(vertexArray);
        }
    }

    return new sf::Texture(finalRender.getTexture());
}

void NodeShape::constructShape(sf::VertexArray &vertexArray, sf::Transform transform, sf::Vector2f size)
{
    if (m_shapeType == Shape::CIRCLE_NORMAL_LAYER) {
        int nbPoint = 50;
        float angle = 3.14159f * 2.f / (float)(nbPoint - 1);
        float cumulAngle = 0.f;
        
        
        vertexArray.append(sf::Vertex(transform.transformPoint(sf::Vector2f()), sf::Color::White));
        for (int i = 0; i < nbPoint; ++i) {
            sf::Vector2f shift(sin(cumulAngle) * size.x / 2.f, cos(cumulAngle) * size.y / 2.f);
            vertexArray.append(sf::Vertex(transform.transformPoint(shift), sf::Color::Black));

            cumulAngle += angle;
        }
    } else if (m_shapeType == Shape::RECTANGLE) {
        float halfX = size.x / 2.f;
        float halfY = size.y / 2.f;

        vertexArray.append(sf::Vertex(transform.transformPoint(sf::Vector2f(-halfX, -halfY)), sf::Color::White));
        vertexArray.append(sf::Vertex(transform.transformPoint(sf::Vector2f(halfX, -halfY)), sf::Color::White));
        vertexArray.append(sf::Vertex(transform.transformPoint(sf::Vector2f(halfX, halfY)), sf::Color::White));
        vertexArray.append(sf::Vertex(transform.transformPoint(sf::Vector2f(-halfX, halfY)), sf::Color::White));
    }
}