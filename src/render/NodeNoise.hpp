#ifndef NODENOISE_H
#define NODENOISE_H

#include "Node.hpp"

class Layer
{
	public:
		float ratio;
		float amplitude;
		float seed;
};

class NodeNoise : public Node
{
	public:
		NodeNoise();
		~NodeNoise();

		static void destroyShader();

		virtual std::string getTypeString();

		void addLayer(float ratio, float amplitude, float seed);
		
	protected:

		virtual sf::Texture *generateTexture(sf::Vector2u renderSize);
		
	private:

		void generateLayer(sf::RenderTexture *result, int renderWidth, int renderHeight, float ratio, float amplitude, float seed);

		static sf::Shader *s_shaderNoise;
		static sf::Shader *s_shaderBicubic;

		std::vector<Layer*> m_layers;
		
};

#endif
