#include "NodeColor.hpp"

sf::Shader *NodeColor::s_shaderColorize(nullptr);

NodeColor::NodeColor() 
: 
    Node::Node(),
    m_inputNode(nullptr)
{
	if (!NodeColor::s_shaderColorize) {
        NodeColor::s_shaderColorize = new sf::Shader();
        if (!NodeColor::s_shaderColorize->loadFromFile("res/shader/colorize.frag", sf::Shader::Fragment)) {
            std::cerr << "shader load error " << std::endl;
        }
    }
}

NodeColor::~NodeColor() 
{
    for (std::vector<ColorizationPoint *>::iterator it = m_points.begin(); it != m_points.end(); ++it) {
        delete *it;
    }
    m_points.clear();
}

void NodeColor::destroyShader()
{
    delete NodeColor::s_shaderColorize; NodeColor::s_shaderColorize = nullptr;
}

std::string NodeColor::getTypeString()
{
    return "Colorize";
}

void NodeColor::setInputNode(Node *inputNode)
{
    m_inputNode = inputNode;
}

void NodeColor::resetColoriztaionPoints()
{
    for (std::vector<ColorizationPoint *>::iterator it = m_points.begin(); it != m_points.end(); ++it) {
        delete *it;
    }
    m_points.clear();
}

void NodeColor::addColorizationPoint(float position, sf::Color color)
{
    ColorizationPoint *point = new ColorizationPoint;
    point->position = position;
    point->color = color;

    m_points.push_back(point);
}

sf::Texture *NodeColor::generateTexture(sf::Vector2u renderSize)
{
    if (!m_inputNode) {
        return nullptr;
    }

    if (!m_inputNode->isGenerate()) {
        m_inputNode->generate(renderSize);
    }
    

    int nbPoint = m_points.size();
    float *positions = new float[nbPoint];
    sf::Glsl::Vec4 *colors = new sf::Glsl::Vec4[nbPoint];

    int index = 0;
    for (std::vector<ColorizationPoint *>::iterator it = m_points.begin(); it != m_points.end(); ++it) {
        ColorizationPoint *point = *it;
        positions[index] = point->position;
        colors[index] = sf::Glsl::Vec4(point->color.r / 255.f, point->color.g / 255.f, point->color.b / 255.f, point->color.a / 255.f);
        index++;
    }

    NodeColor::s_shaderColorize->setUniform("texture", sf::Shader::CurrentTexture);
    NodeColor::s_shaderColorize->setUniform("nbPoint", nbPoint);
    NodeColor::s_shaderColorize->setUniformArray("positions", positions, nbPoint);
    NodeColor::s_shaderColorize->setUniformArray("colors", colors, nbPoint);

    delete positions;
    delete colors;

    sf::Sprite inputSprite(*(m_inputNode->getTexture()));
    inputSprite.setTextureRect(sf::IntRect(0, renderSize.y, renderSize.x, -renderSize.y));
    
    sf::RenderTexture renderColorize;
    renderColorize.create(renderSize.x, renderSize.y);
    renderColorize.draw(inputSprite, NodeColor::s_shaderColorize);

    return new sf::Texture(renderColorize.getTexture());
}

