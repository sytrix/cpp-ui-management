#include "flex/xml.tab.h"

#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <iostream>
#include <string.h>

#include "render/NodeNoise.hpp"
#include "render/NodeColor.hpp"
#include "render/NodeShape.hpp"
#include "render/NodeMix.hpp"
#include "render/NodeMask.hpp"


#define RENDER_WIDTH        1024
#define RENDER_HEIGHT       1024

char *getValueForKey(xml_param_array *paramArray, const char *value) 
{
    if (paramArray) {
        for (unsigned int i = 0; i < paramArray->len; ++i) {
            xml_param *param = paramArray->array[i];
            if (strcmp(param->key, value) == 0) {
                return param->value;
            } 
        }
    }

    return NULL;
}

inline
int hexCharToInt(char c) 
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }

    return 0;
}

sf::Color stringToColor(const char *colorStr) {

    
    if (colorStr && colorStr[0] == '#') {

        int colorLen = strlen(colorStr);
        if (colorLen == 4) {
            return sf::Color(
                hexCharToInt(colorStr[1]) * 17, 
                hexCharToInt(colorStr[2]) * 17, 
                hexCharToInt(colorStr[3]) * 17, 
                255);
        } else if (colorLen == 7) {
            return sf::Color(
                hexCharToInt(colorStr[1]) * 16 + hexCharToInt(colorStr[2]), 
                hexCharToInt(colorStr[3]) * 16 + hexCharToInt(colorStr[4]), 
                hexCharToInt(colorStr[5]) * 16 + hexCharToInt(colorStr[6]), 
                255);
        }
    }

    return sf::Color::Transparent;
}

const char *stringToString(const char *value, const char *defaultValue) {
    if (value) {
        return value;
    }

    return defaultValue;
}

int stringToInt(const char *intStr, int defaultValue = 0) {
    if (intStr) {
        return atoi(intStr);
    }

    return defaultValue;
}

float stringToFloat(const char *floatStr, float defaultValue = 0.f) {
    if (floatStr) {
        return atof(floatStr);
    }

    return defaultValue;
}

bool stringToBool(const char *boolStr, bool defaultValue) {
    if (boolStr) {
        if (strcmp(boolStr, "true")) {
            return true;
        } else if (strcmp(boolStr, "false")) {
            return false;
        } else {
            // TODO : warning
        }
    }

    return defaultValue;
}

std::map<std::string, Node*> nodeMap;
std::map<std::string, int> nodeTypeMap;
std::map<std::string, int> noiseOptionMap;
std::map<std::string, int> colorizeOptionMap;

enum NodeType {NOISE = 1, COLORIZE = 2, SHAPE = 3, MIX = 4, MASK = 5};

enum NodeNoiseOption {LAYER = 1};
enum NodeColorizeOption {POINT = 1};

int enumTypeFromMap(std::map<std::string, int> map, std::string value)
{
    std::map<std::string, int>::iterator iter = map.find(value);
    if (iter != map.end()) {
        int type = iter->second;
        return type;
    }

    return 0;
}

NodeNoise *createNodeNoise(xml_element *element)
{
    NodeNoise *node = new NodeNoise();

    xml_element_array *childs = element->childs;
    int nbElement = childs->len;

    for (int i = 0; i < nbElement; ++i) {
        xml_element *element = childs->array[i];

        int optionType = enumTypeFromMap(noiseOptionMap, element->name);

        if (optionType == NodeNoiseOption::LAYER) {
            char *ratioStringValue = getValueForKey(element->params, "ratio");
            char *amplitudeStringValue = getValueForKey(element->params, "amplitude");

            if (ratioStringValue && amplitudeStringValue) {
                float ratioValue = stringToFloat(ratioStringValue);
                float amplitudeValue = stringToFloat(amplitudeStringValue);
                float seedValue = stringToFloat(getValueForKey(element->params, "seed"), (rand() % 100000) / 100000.f);
                node->addLayer(ratioValue, amplitudeValue, seedValue);

            } else {
                // TODO : error
            }

        } else {
            std::cerr << "unknown option : " << element->name << std::endl;
        }
    }

    return node;
}

NodeColor *createNodeColor(xml_element *element)
{
    NodeColor *node = new NodeColor();

    xml_element_array *childs = element->childs;
    int nbElement = childs->len;

    for (int i = 0; i < nbElement; ++i) {
        xml_element *element = childs->array[i];

        int optionType = enumTypeFromMap(colorizeOptionMap, element->name);

        if (optionType == NodeColorizeOption::POINT) {
            char *positionStringValue = getValueForKey(element->params, "position");
            char *colorStringValue = getValueForKey(element->params, "color");

            if (positionStringValue && colorStringValue) {
                float positionValue = stringToFloat(positionStringValue);
                sf::Color colorValue = stringToColor(colorStringValue);
                node->addColorizationPoint(positionValue, colorValue);

            } else {
                // TODO : error
            }

        } else {
            std::cerr << "unknown option : " << element->name << std::endl;
        }
    }

    char *inputNodeId = getValueForKey(element->params, "input");

    if (inputNodeId) {
        node->setInputNode(nodeMap[inputNodeId]);
    }

    return node;
}

NodeShape *createNodeShape(xml_element *element) {

    NodeShape *node = new NodeShape();

    int nbColumn = stringToInt(getValueForKey(element->params, "nbColumn"), 1);
    int nbLine = stringToInt(getValueForKey(element->params, "nbLine"), 1);
    float marginRatio = stringToFloat(getValueForKey(element->params, "marginRatio"), 0.f);
    float rotation = stringToFloat(getValueForKey(element->params, "rotation"), 0.f);
    
    node->setShapeType(Shape::RECTANGLE);
    node->setNbColumn(nbColumn);
    node->setNbLine(nbLine);
    node->setMarginRatio(marginRatio);
    node->setRotation(rotation);

    return node;
}

NodeMix *createNodeMix(xml_element *element) {

    NodeMix *node = new NodeMix();

    char *inputNodeIdA = getValueForKey(element->params, "inputA");
    char *inputNodeIdB = getValueForKey(element->params, "inputB");

    if (inputNodeIdA) {
        node->setInputNodeA(nodeMap[inputNodeIdA]);
    }

    if (inputNodeIdB) {
        node->setInputNodeB(nodeMap[inputNodeIdB]);
    }

    return node;
}

NodeMask *createNodeMask(xml_element *element) {

    NodeMask *node = new NodeMask();

    char *inputNodeIdMask = getValueForKey(element->params, "inputMask");
    char *inputNodeIdBack = getValueForKey(element->params, "inputBack");
    char *inputNodeIdFront = getValueForKey(element->params, "inputFront");

    if (inputNodeIdMask) {
        node->setInputNodeMask(nodeMap[inputNodeIdMask]);
    }

    if (inputNodeIdBack) {
        node->setInputNodeBack(nodeMap[inputNodeIdBack]);
    }

    if (inputNodeIdFront) {
        node->setInputNodeFront(nodeMap[inputNodeIdFront]);
    }

    return node;
}

void makeNodeMap(xml_element_array *root)
{
    int nbElement = root->len;

    for (int i = 0; i < nbElement; ++i) {

        xml_element *element = root->array[i];
        
        int nodeType = enumTypeFromMap(nodeTypeMap, element->name);
        const char *idValue = stringToString(getValueForKey(element->params, "id"), std::string("default_" + std::to_string(i)).c_str());
        Node *node = nullptr;

        if (nodeType == NodeType::NOISE) {
            node = createNodeNoise(element);
        } else if (nodeType == NodeType::COLORIZE) {
            node = createNodeColor(element);
        } else if (nodeType == NodeType::SHAPE) {
            node = createNodeShape(element);
        } else if (nodeType == NodeType::MIX) {
            node = createNodeMix(element);
        } else if (nodeType == NodeType::MASK) {
            node = createNodeMask(element);
        } else {
            std::cerr << "unknown element : " << element->name << std::endl;
        }

        if (node) {
            nodeMap[idValue] = node;
        }
    }
}

int main(int argc, char *argv[])
{
    xml_element_array *root = parseXmlFile("res/render.xml");


    srand(time(0));

    nodeTypeMap["noise"] = NodeType::NOISE;
    nodeTypeMap["colorize"] = NodeType::COLORIZE;
    nodeTypeMap["shape"] = NodeType::SHAPE;
    nodeTypeMap["mix"] = NodeType::MIX;
    nodeTypeMap["mask"] = NodeType::MASK;

    noiseOptionMap["layer"] = NodeNoiseOption::LAYER;

    colorizeOptionMap["point"] = NodeColorizeOption::POINT;

    makeNodeMap(root);
    
    sf::Clock clockTotal;
    clockTotal.restart();

    sf::Clock internClock;
    internClock.restart();
    sf::RenderTexture renderTextureA;
    renderTextureA.create(1, 1);
    renderTextureA.clear();
    std::cout << "first render time : " << internClock.getElapsedTime().asSeconds() << std::endl;
    
    internClock.restart();
    
    for (std::map<std::string, Node*>::iterator it = nodeMap.begin(); it != nodeMap.end(); ++it) {
        Node *node = it->second;
        node->generate(sf::Vector2u(RENDER_WIDTH, RENDER_HEIGHT));
    }

    std::cout << "generation time : " << internClock.getElapsedTime().asSeconds() << std::endl;

    std::cout << "-> total time : " << clockTotal.getElapsedTime().asSeconds() << std::endl;

    for (std::map<std::string, Node*>::iterator it = nodeMap.begin(); it != nodeMap.end(); ++it) {
        std::string id = it->first;
        Node *node = it->second;
        std::cout << "generate \"" << id << "\" : " << node->getTypeString() << std::endl;
        if (node->isGenerate()) {
            sf::Image imageNoise(node->getTexture()->copyToImage());
            imageNoise.saveToFile(id + ".png");
        } else {
            std::cout << "texture is not generated" << std::endl;
        }
    }


    NodeNoise::destroyShader();
    NodeColor::destroyShader();

    freeXmlStruct(root);

    return 0;
}


