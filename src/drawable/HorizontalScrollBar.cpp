#include "HorizontalScrollBar.hpp"

#include "RoundedRectangle.hpp"

HorizontalScrollBar::HorizontalScrollBar() 
: ScrollBar::ScrollBar()
{
	
}

HorizontalScrollBar::~HorizontalScrollBar() 
{
	
}

void HorizontalScrollBar::setBarSize(float contentSize, float viewSize)
{
    ScrollBar::setBarSize(contentSize, viewSize);

    if (!m_contentBar) {    
        m_contentBar = new RoundedRectangle();
        m_viewBar = new RoundedRectangle();
    }

    m_contentBar->makeRoundedRectangle(this->contentBarLength(), SCROLLBAR_THICKNESS, SCROLLBAR_THICKNESS / 2);
    m_viewBar->makeRoundedRectangle(this->viewBarLength(), SCROLLBAR_THICKNESS, SCROLLBAR_THICKNESS / 2);

    m_contentBar->setFillColor(sf::Color(m_color.r, m_color.g, m_color.b, 64));
    m_viewBar->setFillColor(sf::Color(m_color.r, m_color.g, m_color.b, 255));

    this->updateViewOffsetContentBar();
    
}

void HorizontalScrollBar::setOffset(float offset)
{
    ScrollBar::setOffset(offset);

    if (m_contentBar) {
        this->updateViewOffsetContentBar();
    }
}

void HorizontalScrollBar::setPosition(float x, float y)
{
    m_contentBar->setPosition(sf::Vector2f(x + SCROLLBAR_PADDING, y - SCROLLBAR_THICKNESS - SCROLLBAR_PADDING));
    this->updateViewOffsetContentBar();
}

bool HorizontalScrollBar::updateEvent(sf::Event &event)
{
    return false;
}

void HorizontalScrollBar::updateViewOffsetContentBar()
{
    sf::Vector2f contentPosition = m_contentBar->getPosition();
    m_viewBar->setPosition(contentPosition.x + this->viewBarOffset(), contentPosition.y);
}