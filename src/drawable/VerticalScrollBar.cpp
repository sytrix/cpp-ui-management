#include "VerticalScrollBar.hpp"

#include "RoundedRectangle.hpp"

VerticalScrollBar::VerticalScrollBar() 
: ScrollBar::ScrollBar()
{
	
}

VerticalScrollBar::~VerticalScrollBar() 
{
	
}

void VerticalScrollBar::setBarSize(float contentSize, float viewSize)
{
    ScrollBar::setBarSize(contentSize, viewSize);

    if (!m_contentBar) {    
        m_contentBar = new RoundedRectangle();
        m_viewBar = new RoundedRectangle();
    }

    m_contentBar->makeRoundedRectangle(SCROLLBAR_THICKNESS, this->contentBarLength(), SCROLLBAR_THICKNESS / 2);
    m_viewBar->makeRoundedRectangle(SCROLLBAR_THICKNESS, this->viewBarLength(), SCROLLBAR_THICKNESS / 2);

    m_contentBar->setFillColor(sf::Color(m_color.r, m_color.g, m_color.b, 64));
    m_viewBar->setFillColor(sf::Color(m_color.r, m_color.g, m_color.b, 255));

    this->updateViewOffsetContentBar();
    
}

void VerticalScrollBar::setOffset(float offset)
{
    ScrollBar::setOffset(offset);

    if (m_contentBar) {
        this->updateViewOffsetContentBar();
    }
}

void VerticalScrollBar::setPosition(float x, float y)
{
    m_contentBar->setPosition(sf::Vector2f(x - SCROLLBAR_THICKNESS - SCROLLBAR_PADDING, y + SCROLLBAR_PADDING));
    this->updateViewOffsetContentBar();
}

bool VerticalScrollBar::updateEvent(sf::Event &event)
{
    return false;
}

void VerticalScrollBar::updateViewOffsetContentBar()
{
    sf::Vector2f contentPosition = m_contentBar->getPosition();
    m_viewBar->setPosition(contentPosition.x, contentPosition.y + this->viewBarOffset());
}

