#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include <SFML/Graphics.hpp>

class RoundedRectangle;

#define SCROLLBAR_PADDING		10.f
#define SCROLLBAR_THICKNESS		8.f



class ScrollBar : public sf::Drawable
{
	public:
		ScrollBar();
		~ScrollBar();

		void setColor(sf::Color color);

		virtual void setBarSize(float contentSize, float viewSize);
		virtual void setOffset(float offset);
		virtual void setPosition(float x, float y);

		float getOffset();

		virtual bool updateEvent(sf::Event &event);
		
	protected:

		float contentBarLength();
		float viewBarLength();
		float viewBarOffset();

		virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
		

		RoundedRectangle *m_contentBar;
		RoundedRectangle *m_viewBar;
		sf::Color m_color;
		float m_contentSize;
		float m_viewSize;
		float m_offset;

	private:
		

};

#endif
