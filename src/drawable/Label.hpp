#ifndef DEF_LABEL_H
#define DEF_LABEL_H

#include <SFML/Graphics.hpp>

class Label : public sf::Text
{
    public:

        Label(const sf::String& string, const sf::Font& font, unsigned int characterSize = 30);

        void setString(const sf::String &string);
        const sf::String &getString();
        void fit(float width, float height = -1.f, int mode = 0);

    private:
        sf::String m_originalString;


};

#endif

