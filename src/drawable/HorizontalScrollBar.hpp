#ifndef HORIZONTALSCROLLBAR_H
#define HORIZONTALSCROLLBAR_H

#include "ScrollBar.hpp"

class HorizontalScrollBar : public ScrollBar
{
	public:
		HorizontalScrollBar();
		~HorizontalScrollBar();
		
		virtual void setBarSize(float contentSize, float viewSize);
		virtual void setOffset(float offset);
		virtual void setPosition(float x, float y);
		virtual bool updateEvent(sf::Event &event);

	protected:

		virtual void updateViewOffsetContentBar();
		
	private:
		
};

#endif
