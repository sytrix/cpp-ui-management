#include "Label.hpp"

#include <vector>
#include <string>
#include <iostream>


Label::Label(const sf::String& string, const sf::Font& font, unsigned int characterSize) 
: 
    Text::Text(string, font, characterSize), 
    m_originalString(string)
{

}

void Label::setString(const sf::String &string)
{
    m_originalString = string;
    Text::setString(m_originalString);
}

const sf::String &Label::getString()
{
    return m_originalString;
}

void Label::fit(float width, float height, int mode)
{
    const sf::Font *font = getFont();
    std::wstring transformedString = m_originalString.toWideString();
    const sf::Uint32 style = getStyle();
    const unsigned int characterSize = getCharacterSize(); 
    const float letterSpacingFactor = getLetterSpacing(); 
    const float lineSpacingFactor = getLineSpacing(); 
    std::vector<sf::Vector2f> listPosition;

    // Make sure that we have a valid font
    if (!font)
        return;

    // Adjust the index if it's out of range

    // Precompute the variables needed by the algorithm
    bool  isBold          = style & Text::Bold;
    float whitespaceWidth = font->getGlyph(L' ', characterSize, isBold).advance;
    float letterSpacing   = ( whitespaceWidth / 3.f ) * ( letterSpacingFactor - 1.f );
    whitespaceWidth      += letterSpacing;
    float lineSpacing     = font->getLineSpacing(characterSize) * lineSpacingFactor;
    float pointsSpacing = (font->getGlyph(L'.', characterSize, isBold).advance + letterSpacing) * 3;

    // Compute the position
    sf::Vector2f position;
    sf::Uint32 prevChar = 0;
    std::size_t beginWordIndex = 0;
    float beginWordX = 0;
    bool breakParagraphe = false;
    int lineWord = 0;
    int insertedChar = 0;

    if ((height != -1.f && height <= 0.f) || width <= 0.f) {
        Text::setString("");
        return;
    }
    
    for (std::size_t i = 0; i < m_originalString.getSize() && !breakParagraphe; ++i)
    {
        if (height > 0.0f && position.y + lineSpacing * 2 > height && position.x + pointsSpacing * 2 > width && mode == 0) {
            breakParagraphe = true;
            transformedString = transformedString.substr(0, i);
            transformedString.append(L"...");
        
        } else {
            sf::Uint32 curChar = m_originalString[i];

            // Apply the kerning offset
            position.x += font->getKerning(prevChar, curChar, characterSize);
            prevChar = curChar;

            if(curChar != ' ' && curChar != '\t' && curChar != '\n') {
                // For regular characters, add the advance offset of the glyph
                position.x += font->getGlyph(curChar, characterSize, isBold).advance + letterSpacing;
            }

            if(position.x > width) {
                if(lineWord == 0) {
                    transformedString.insert(i + insertedChar, L"\n");
                    insertedChar++;
                    position.y += lineSpacing; 
                    position.x = position.x - width + whitespaceWidth; 
                } else {
                    lineWord = 0;
                    transformedString[beginWordIndex + insertedChar] = L'\n';
                    position.y += lineSpacing;
                    position.x = position.x - beginWordX + whitespaceWidth; 
                }
            }

            // Handle special characters
            switch (curChar)
            {
                case ' ':
                    lineWord++;
                    position.x += whitespaceWidth;
                    beginWordX = position.x;
                    beginWordIndex = i;
                    continue;
                case '\t':
                    lineWord++;
                    position.x += whitespaceWidth * 4;
                    beginWordX = position.x;
                    beginWordIndex = i; 
                    continue;
                case '\n': 
                    lineWord = 0;
                    position.y += lineSpacing; 
                    position.x = 0;
                    beginWordX = position.x;
                    beginWordIndex = i; 
                    continue;
            }

            
        }
    }

    Text::setString(transformedString);
}

