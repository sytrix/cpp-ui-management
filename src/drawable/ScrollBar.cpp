#include "ScrollBar.hpp"

#include "RoundedRectangle.hpp"



ScrollBar::ScrollBar() 
: 
    m_contentBar(nullptr),
    m_viewBar(nullptr),
    m_color(sf::Color::Transparent)
{
	
}

ScrollBar::~ScrollBar() 
{
	delete m_contentBar; m_contentBar = NULL;
    delete m_viewBar; m_viewBar = NULL;
}

void ScrollBar::setColor(sf::Color color)
{
    m_color = color;
}

void ScrollBar::setBarSize(float contentSize, float viewSize)
{
    m_contentSize = contentSize;
    m_viewSize = viewSize;
}

void ScrollBar::setOffset(float offset)
{
    m_offset = offset;

    if (m_offset < 0) {
        m_offset = 0;
    } else if (m_offset + m_viewSize >= m_contentSize) {
        m_offset = m_contentSize - m_viewSize;
    }
}

void ScrollBar::setPosition(float x, float y)
{

}

float ScrollBar::getOffset()
{
    return m_offset;
}

bool ScrollBar::updateEvent(sf::Event &event)
{
    return false;
}

float ScrollBar::contentBarLength()
{
    return m_viewSize - SCROLLBAR_PADDING * 2;
}

float ScrollBar::viewBarLength()
{
    float barLen = this->contentBarLength() * m_viewSize / m_contentSize;

    if (barLen < SCROLLBAR_THICKNESS) {
        barLen = SCROLLBAR_THICKNESS;
    }

    return barLen;
}

float ScrollBar::viewBarOffset()
{
    return this->contentBarLength() * m_offset / m_contentSize;
}

void ScrollBar::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (m_contentBar) {
        target.draw(*m_contentBar, states);
    }

    if (m_viewBar) {
        target.draw(*m_viewBar, states);
    }
}