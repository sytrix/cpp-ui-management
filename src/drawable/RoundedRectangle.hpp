#ifndef ROUNDEDRECTANGLE_H
#define ROUNDEDRECTANGLE_H

#include <SFML/Graphics.hpp>

class RoundedRectangle : public sf::ConvexShape
{
	public:
		RoundedRectangle();
		~RoundedRectangle();

		void makeRoundedRectangle(float width, float height, float cornerRadius);
		void makeRoundedRectangle(float width, float height, float cornerRadiusX, float cornerRadiusY);

	protected:
		
	private:
		
};

#endif
