#ifndef VERTICALSCROLLBAR_H
#define VERTICALSCROLLBAR_H

#include "ScrollBar.hpp"

class VerticalScrollBar : public ScrollBar
{
	public:
		VerticalScrollBar();
		~VerticalScrollBar();

		virtual void setBarSize(float contentSize, float viewSize);
		virtual void setOffset(float offset);
		virtual void setPosition(float x, float y);
		virtual bool updateEvent(sf::Event &event);

	protected:

		virtual void updateViewOffsetContentBar();
		
	private:
		
};

#endif
