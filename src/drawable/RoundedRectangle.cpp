#include "RoundedRectangle.hpp"

#include <cmath>



RoundedRectangle::RoundedRectangle() 
: ConvexShape::ConvexShape()
{
	this->setFillColor(sf::Color::Transparent);
}

RoundedRectangle::~RoundedRectangle() 
{
	
}

void RoundedRectangle::makeRoundedRectangle(float width, float height, float cornerRadius)
{
    this->makeRoundedRectangle(width, height, cornerRadius, cornerRadius);
}

void RoundedRectangle::makeRoundedRectangle(float width, float height, float cornerRadiusX, float cornerRadiusY)
{
    if (cornerRadiusX == 0.f && cornerRadiusY == 0.f) {
        this->setPointCount(4);
        this->setPoint(0, sf::Vector2f(0, 0));
        this->setPoint(1, sf::Vector2f(width, 0));
        this->setPoint(2, sf::Vector2f(width, height));
        this->setPoint(3, sf::Vector2f(0, height));
    } else {
        const float pi = 3.14159f;
        int pointByCorner = 5;
        float deltaAngle = pi / 2.f / (pointByCorner - 1);

        this->setPointCount(pointByCorner * 4);

        for (int i = 0; i < pointByCorner; i++) {
            float angle = (float)i * deltaAngle;
            float cosA = cos(angle);
            float sinA = sin(angle);
            float shiftCosX = cosA * cornerRadiusX;
            float shiftCosY = cosA * cornerRadiusY;
            float shiftSinX = sinA * cornerRadiusX;
            float shiftSinY = sinA * cornerRadiusY;

            this->setPoint(i,                       sf::Vector2f(cornerRadiusX - shiftCosX, cornerRadiusY - shiftSinY));
            this->setPoint(pointByCorner + i,       sf::Vector2f(width - cornerRadiusX + shiftSinX, cornerRadiusY - shiftCosY));
            this->setPoint(pointByCorner * 2 + i,   sf::Vector2f(width - cornerRadiusX + shiftCosX, height - cornerRadiusY + shiftSinY));
            this->setPoint(pointByCorner * 3 + i,   sf::Vector2f(cornerRadiusX - shiftSinX, height - cornerRadiusY + shiftCosY));
        }
    }
}