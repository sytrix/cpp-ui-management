/*
#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <string.h>

//#include "flex/xml/XmlElement.h"
//#include "flex/xml/XmlParam.h"
#include <string>
#include "flex/xml.tab.h"

#include "view/Label.hpp"
#include "view/DrawableView.hpp"
#include "view/LabelView.hpp"
#include "view/HorizontalListView.hpp"
#include "view/VerticalListView.hpp"
#include "view/ButtonView.hpp"
#include "view/ListView.hpp"
#include "view/ButtonLabelView.hpp"

using namespace sf;

sf::Font fontTitle;
sf::Font fontText;


char *getValueForKey(xml_param_array *paramArray, const char *value) 
{
    if (paramArray) {
        for (unsigned int i = 0; i < paramArray->len; ++i) {
            xml_param *param = paramArray->array[i];
            if (strcmp(param->key, value) == 0) {
                return param->value;
            } 
        }
    }

    return NULL;
}

inline
int hexCharToInt(char c) 
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }

    return 0;
}

sf::Color stringToColor(const char *colorStr) {

    
    if (colorStr && colorStr[0] == '#') {

        int colorLen = strlen(colorStr);
        if (colorLen == 4) {
            return sf::Color(
                hexCharToInt(colorStr[1]) * 17, 
                hexCharToInt(colorStr[2]) * 17, 
                hexCharToInt(colorStr[3]) * 17, 
                255);
        } else if (colorLen == 7) {
            return sf::Color(
                hexCharToInt(colorStr[1]) * 16 + hexCharToInt(colorStr[2]), 
                hexCharToInt(colorStr[3]) * 16 + hexCharToInt(colorStr[4]), 
                hexCharToInt(colorStr[5]) * 16 + hexCharToInt(colorStr[6]), 
                255);
        }
    }

    return sf::Color::Transparent;
}

LabelView::Alignement stringToLabelViewAlignement(const char *alignementStr, bool isHorizontalAlignement) {
    if (isHorizontalAlignement) {
        if (alignementStr) {
            if (strcmp(alignementStr, "left") == 0) {
                return LabelView::Alignement::Left;
            } else if (strcmp(alignementStr, "center") == 0) {
                return LabelView::Alignement::Center;
            } else if (strcmp(alignementStr, "right") == 0) {
                return LabelView::Alignement::Right;
            }
        }

        return LabelView::Left;
    } else {
        if (alignementStr) {
            if (strcmp(alignementStr, "top") == 0) {
                return LabelView::Top;
            } else if (strcmp(alignementStr, "center") == 0) {
                return LabelView::Center;
            } else if (strcmp(alignementStr, "bottom") == 0) {
                return LabelView::Bottom;
            }
        }

        return LabelView::Top;
    }
}

HorizontalListView::Alignement stringToHorizontalListViewAlignement(const char *alignementStr) {
    if (alignementStr) {
        if (strcmp(alignementStr, "left") == 0) {
            return HorizontalListView::Alignement::Left;
        } else if (strcmp(alignementStr, "center") == 0) {
            return HorizontalListView::Alignement::Center;
        } else if (strcmp(alignementStr, "right") == 0) {
            return HorizontalListView::Alignement::Right;
        }
    }

    return HorizontalListView::Alignement::Left;
}

VerticalListView::Alignement stringToVerticalListViewAlignement(const char *alignementStr) {
    if (alignementStr) {
        if (strcmp(alignementStr, "top") == 0) {
            return VerticalListView::Alignement::Top;
        } else if (strcmp(alignementStr, "center") == 0) {
            return VerticalListView::Alignement::Center;
        } else if (strcmp(alignementStr, "bottom") == 0) {
            return VerticalListView::Alignement::Bottom;
        }
    }

    return VerticalListView::Alignement::Top;
}


int stringToInt(const char *intStr) {
    if (intStr) {
        return atoi(intStr);
    }

    return 0;
}

float stringToFloat(const char *floatStr) {
    if (floatStr) {
        return atof(floatStr);
    }

    return 0;
}

bool stringToBool(const char *boolStr, bool defaultValue) {
    if (boolStr) {
        if (strcmp(boolStr, "true")) {
            return true;
        }
    }

    return defaultValue;
}

DrawableView *makeDrawableView(xml_element *element)
{
    DrawableView *drawableView = nullptr;

    if (strcmp(element->name, "button") == 0)
    {
        char *buttonTitle = getValueForKey(element->params, "title");
        ButtonView *buttonView = nullptr;

        if (buttonTitle) {
            ButtonLabelView *buttonLabelView = nullptr;
            int fontSize = stringToInt(getValueForKey(element->params, "fontSize"));

            if (fontSize > 0) {
                buttonLabelView = new ButtonLabelView(buttonTitle, fontTitle, fontSize);
                buttonView = buttonLabelView;
            } else {
                buttonLabelView = new ButtonLabelView(buttonTitle, fontTitle);
            }

            sf::Color buttonLabelViewDefaultColor = stringToColor(getValueForKey(element->params, "defaultTitleColor"));
            sf::Color buttonLabelViewOverColor = stringToColor(getValueForKey(element->params, "overTitleColor"));
            LabelView::Alignement buttonLabelViewVerticalAlignement = stringToLabelViewAlignement(getValueForKey(element->params, "horizontalAlignement"), true);
            LabelView::Alignement buttonLabelViewHorizontalAlignement = stringToLabelViewAlignement(getValueForKey(element->params, "verticalAlignement"), false);

            if (buttonLabelViewDefaultColor.a != 0) { buttonLabelView->setButtonLabelDefaultColor(buttonLabelViewDefaultColor); }
            if (buttonLabelViewOverColor.a != 0) { buttonLabelView->setButtonLabelOverColor(buttonLabelViewOverColor); }
            buttonLabelView->setVerticalAlignement(buttonLabelViewVerticalAlignement);
            buttonLabelView->setHorizontalAlignement(buttonLabelViewHorizontalAlignement);
            

            buttonView = buttonLabelView;
        } else {

        }

        sf::Color buttonViewDefaultColor = stringToColor(getValueForKey(element->params, "defaultColor"));
        sf::Color buttonViewOverColor = stringToColor(getValueForKey(element->params, "overColor"));

        if (buttonViewDefaultColor.a != 0) { buttonView->setButtonDefaultColor(buttonViewDefaultColor); }
        if (buttonViewOverColor.a != 0) { buttonView->setButtonOverColor(buttonViewOverColor); }

        drawableView = buttonView;
    } else if (strcmp(element->name, "hstack") == 0 || strcmp(element->name, "vstack") == 0) {

        ListView *listView = nullptr;
        float space = stringToFloat(getValueForKey(element->params, "space"));

        if (strcmp(element->name, "hstack") == 0) {
            HorizontalListView *horizontalListView = new HorizontalListView(space);

            HorizontalListView::Alignement horizontalListViewAlignement = stringToHorizontalListViewAlignement(getValueForKey(element->params, "alignement"));
            
            horizontalListView->setAlignement(horizontalListViewAlignement);

            listView = horizontalListView;

        } else if (strcmp(element->name, "vstack") == 0) {
            VerticalListView *verticalListView = new VerticalListView(space);

            VerticalListView::Alignement verticalListViewAlignement = stringToVerticalListViewAlignement(getValueForKey(element->params, "alignement"));
            
            verticalListView->setAlignement(verticalListViewAlignement);

            listView = verticalListView;
            
        }

        xml_element_array *childs = element->childs;
        xml_element **elements = childs->array;

        for (unsigned int i = 0; i < childs->len; ++i) {
            DrawableView *childDrawableView = makeDrawableView(elements[i]);
            listView->addSubview(childDrawableView);
        }

        std::cout << "childs len : " << childs->len << std::endl;

        drawableView = listView;
    }

    if (drawableView) {

        sf::Color drawableViewOutlineColor = stringToColor(getValueForKey(element->params, "outlineColor"));
        float drawableViewOutlineTickness = stringToFloat(getValueForKey(element->params, "outlineTickness"));
        int drawableViewWidth = stringToInt(getValueForKey(element->params, "width"));
        int drawableViewHeight = stringToInt(getValueForKey(element->params, "height"));
        int drawableViewVerticalPadding = stringToInt(getValueForKey(element->params, "verticalPadding"));
        int drawableViewHorizontalPadding = stringToInt(getValueForKey(element->params, "horizontalPadding"));
        bool drawableViewFitWidthToSuperview = stringToBool(getValueForKey(element->params, "fitWidthToParent"), false);
        bool drawableViewFitHeightToSuperview = stringToBool(getValueForKey(element->params, "fitHeightToParent"), false);

        if (drawableViewOutlineColor.a != 0) { drawableView->setViewOutlineColor(drawableViewOutlineColor); } 
        if (drawableViewOutlineTickness > 0.f) { drawableView->setViewOutlineThickness(drawableViewOutlineTickness); }
        if (drawableViewWidth > 0 || drawableViewWidth > 0) { drawableView->setViewSize(drawableViewWidth, drawableViewHeight); }
        if (drawableViewVerticalPadding > 0 || drawableViewHorizontalPadding > 0) { drawableView->setViewPadding(drawableViewVerticalPadding, drawableViewHorizontalPadding); }
        drawableView->setFitWidthToSuperView(drawableViewFitWidthToSuperview);
        drawableView->setFitHeightToSuperView(drawableViewFitHeightToSuperview);

        std::cout << "I was there" << std::endl;

        return drawableView;
    }

    return nullptr;
}

void makeRoot(xml_element_array *xmlRoot, ListView *listView, sf::Vector2i displaySize)
{
    xml_element **elements = xmlRoot->array;

    for (unsigned int i = 0; i < xmlRoot->len; ++i) {
        DrawableView *drawableView = makeDrawableView(elements[i]);
        listView->addSubview(drawableView);
    }

    listView->setViewSize(displaySize.x, displaySize.y);
    //listView->layoutContent();
}

int main(int argc, char *argv[])
{
    if (!fontTitle.loadFromFile("res/font/theboldfont.ttf"))
        return EXIT_FAILURE;

    if (!fontText.loadFromFile("res/font/LANENAR_.ttf"))
        return EXIT_FAILURE;

    // Create the main window
    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");

    xml_element_array *root = parseXmlFile("res/interface.xml");

    ListView listView;

    makeRoot(root, &listView, sf::Vector2i(800, 600));

    sf::Clock clock;
    //float width = 400;
    //textLabel->fit(width, texture.getSize().y * 0.2f);

    window.setFramerateLimit(60);
    int i = 0;

    // Start the game loop
    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed) {
                if(event.key.code == sf::Keyboard::Left) {
                }
            }
            if (event.type == sf::Event::MouseMoved) {
                float mouseX = event.mouseMove.x;
                float mouseY = event.mouseMove.y;

                listView.eventMouseOver(mouseX, mouseY);
            }
        }

        if(clock.getElapsedTime().asSeconds() > 1.f) {
            //std::cout << "fps : " << i << std::endl;
            clock.restart();
            i = 0;
        }
        i++;

        // Clear screen
        window.clear(sf::Color::White);
        // Draw the string
        window.draw(listView);
        // Update the window
        window.display();
    }

    freeXmlStruct(root);

    return EXIT_SUCCESS;
}
*/