
INCLUDE_SFML=-I E:/mingw64/include
INCLUDE_MINGW=-B E:/mingw64/include
DEBUG=-O3 -g

REMOVE=powershell.exe ./Remove-If-Exist.ps1

LIST_VIEW_FILE= \
		obj/view/DrawableView.o \
		obj/view/LabelView.o \
		obj/view/ListView.o \
		obj/view/HorizontalListView.o \
		obj/view/VerticalListView.o \
		obj/view/ButtonView.o \
		obj/view/ButtonLabelView.o \
		obj/view/ImageView.o \
		obj/view/UIManager.o  

LIST_DRAWABLE_FILE= \
		obj/drawable/Label.o \
		obj/drawable/RoundedRectangle.o \
		obj/drawable/ScrollBar.o \
		obj/drawable/HorizontalScrollBar.o \
		obj/drawable/VerticalScrollBar.o 

LIST_XML_FILE= \
		obj/flex/xml/XmlElement.o \
		obj/flex/xml/XmlParam.o \
		obj/flex/xml/XmlUtils.o \
		obj/flex/xml.o

LIST_RENDER_FILE= \
		obj/render/Node.o \
		obj/render/NodeNoise.o \
		obj/render/NodeColor.o \
		obj/render/NodeShape.o \
		obj/render/NodeMix.o \
		obj/render/NodeMask.o




all : bin/render_test.exe bin/ui2_test.exe bin/xml_test.exe bin/browser_test.exe 

clean_ui :
	$(REMOVE) bin/ui_test.exe
	$(REMOVE) bin/ui2_test.exe
	$(REMOVE) obj/ui_test.o
	$(REMOVE) obj/ui2_test.o
	$(REMOVE) obj/view/*.o
	$(REMOVE) obj/drawable/*.o

clean_render :
	$(REMOVE) bin/render_test.exe
	$(REMOVE) bin/render2_test.exe
	$(REMOVE) obj/render_test.o
	$(REMOVE) obj/render2_test.o
	$(REMOVE) obj/render/*.o

clean_xml :
	$(REMOVE) bin/xml_test.exe
	$(REMOVE) obj/xml_test.o
	$(REMOVE) obj/flex/xml/*.o
	$(REMOVE) obj/flex/*.o
	$(REMOVE) obj/flex/*.c

clean_browser : 
	$(REMOVE) bin/browser_test.exe
	$(REMOVE) obj/browser_test.o



bin/browser_test.exe : $(LIST_VIEW_FILE) $(LIST_XML_FILE) obj/browser_test.o
	g++ $(DEBUG) -o $@ $^ -lsfml-window -lsfml-system -lsfml-graphics

obj/browser_test.o : src/browser_test.cpp
	g++ $(INCLUDE_SFML) $(DEBUG) -c $< -o $@



bin/render2_test.exe : $(LIST_XML_FILE) $(LIST_RENDER_FILE) obj/render2_test.o
	g++ $(DEBUG) -o $@ $^ -lsfml-window -lsfml-system -lsfml-graphics

obj/render2_test.o : src/render2_test.cpp
	g++ $(INCLUDE_SFML) $(DEBUG) -c $< -o $@



bin/ui2_test.exe : $(LIST_VIEW_FILE) $(LIST_DRAWABLE_FILE) obj/ui2_test.o
	g++ $(DEBUG) -o $@ $^ -lsfml-window -lsfml-system -lsfml-graphics

obj/ui2_test.o : src/ui2_test.cpp
	g++ $(INCLUDE_SFML) $(DEBUG) -c $< -o $@

obj/%.o : src/%.cpp src/%.hpp
	g++ $(INCLUDE_SFML) $(DEBUG) -c $< -o $@



bin/xml_test.exe : $(LIST_XML_FILE) obj/xml_test.o
	g++ $^ -o $@

obj/xml_test.o : src/xml_test.cpp
	g++ $(INCLUDE_MINGW) $(DEBUG) -c $< -o $@

obj/flex/%.o : obj/flex/%.c
	g++ $(INCLUDE_MINGW) $(DEBUG) -c $< -o $@

obj/flex/%.c : src/flex/%.y src/flex/%.l 
	bison --yacc -o"obj/flex/xml.tab.c" src/flex/xml.y
	flex -o"$@" src/flex/xml.l

obj/flex/xml/%.o : src/flex/xml/%.c src/flex/xml/%.h
	g++ $(INCLUDE_MINGW) $(DEBUG) -c $< -o $@

